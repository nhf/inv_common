﻿#region

using Inv.Common.Adapter.InventorAdapters;
using Inventor;

#endregion

namespace Inv.Common._Core
{
    public abstract class BaseSetup
    {
        #region ButtonDefinitions

        protected abstract string Name { get; }
        protected abstract string Desc { get; }

        #endregion

        #region UI Bindings

        protected abstract string PanelInternalName { get; }
        protected abstract string PanelName { get; }

        protected abstract string AddInGUID { get; }

        protected abstract Application Inv { get; }

        protected void BindButtonToPartTab(string ribbonTabName, bool useLargeIcon = false, bool overflowItem = false,
            params ButtonDefinition[] buttons)
        {
            RibbonTab ribbonTab = Inv.GetPartRibbonTab(ribbonTabName);
            BindButton(ribbonTab, PanelInternalName + "_Part", useLargeIcon, overflowItem, buttons);
        }

        protected void BindButtonToAsmTab(string ribbonTabName, bool useLargeIcon = false, bool overflowItem = false,
            params ButtonDefinition[] buttons)
        {
            RibbonTab ribbonTab = Inv.GetAsmRibbonTab(ribbonTabName);
            BindButton(ribbonTab, PanelInternalName + "_Part", useLargeIcon, overflowItem, buttons);
        }

        protected void BindButton(RibbonTab ribbonTab, string panelInternalName, bool useLargeIcon = false,
            bool overflowItem = false, params ButtonDefinition[] buttons)
        {
            RibbonPanel rp = RibbonProvider.GetOrAddPanel(ribbonTab.RibbonPanels, PanelName, panelInternalName,
                AddInGUID);
            foreach (ButtonDefinition button in buttons)
            {
                if (overflowItem)
                {
                    rp.SlideoutControls.AddButton(button);
                    continue;
                }
                rp.CommandControls.AddButton(button, useLargeIcon);
            }
        }

        #endregion

        #region Application & Document Events

        // Application Events

        public virtual void AddApplicationEvents(ApplicationEvents applicationEvents)
        {
            // No default implementation
        }

        public virtual void RemoveApplicationEvents(ApplicationEvents applicationEvents)
        {
            // No default implementation
        }

        // Document Events

        // TODO

        #endregion
    }
}