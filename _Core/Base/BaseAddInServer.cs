﻿using Inv.Common._Core.Adn;
using Inv.Common.Adapter.InventorAdapters;
using Inventor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common._Core.Base
{
    public abstract class BaseAddInServer : ApplicationAddInServer
    {
        // Inventor application object.
        public Application invApp;

        protected readonly IList<AdnCommand> commands = new List<AdnCommand>();

        #region GUID

        public string AddInClientId()
        {
            var guid = "";
            try
            {
                // Attempt to get the AddInServer GUID via reflection
                Assembly assembly = GetType().Assembly;
                var attr = (GuidAttribute)assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0];
                guid = "{" + attr.Value + "}";
            }
            catch (Exception)
            {
                // TODO: throw
            }
            return guid;
        }

        #endregion

        #region Reflection

        private static string _assemblyName;

        private static string AssemblyName
        {
            get
            {
                if (!string.IsNullOrEmpty(_assemblyName)) return _assemblyName;

                _assemblyName = Assembly.GetExecutingAssembly().Location;
                _assemblyName = _assemblyName.Remove(_assemblyName.Length - 4, 4);
                return _assemblyName;
            }
        }

        #endregion

        public abstract void OnActivate();

        public void Activate(ApplicationAddInSite addInSiteObject, bool firstTime)
        {
            // Initialize AddIn members.
            invApp = InventorProvider.GetInstanceInProcess(addInSiteObject);

            if (!firstTime) return;
            OnActivate();

            // Add the commands
            foreach (AdnCommand command in commands)
            {
                AdnCommand.AddCommand(command);
            }

            //Check for ribbon.override.xml -> provides the user a way to override the 
            //ribbon settings embedded in the add-in once installed.
            //The user can just place an xml file named "asmName.ribbons.override.xml"
            //at the same location than the add-in dll and replacing "asmName" with the 
            //actual name of the assembly dll

            Type addInType = GetType();
            string addInAssemblyName = addInType.Assembly.GetName().Name;

            //Check for assembly.ribbon.override.xml first
            if (System.IO.File.Exists(AssemblyName + ".ribbon.override.xml"))
            {
                AdnRibbonBuilder.CreateRibbonFromFile(
                    invApp,
                    addInType,
                    addInAssemblyName + ".ribbon.override.xml");
            }
            else
            {
                AdnRibbonBuilder.CreateRibbon(
                    invApp,
                    addInType,
                    addInAssemblyName + ".Resources.ribbon.xml");
            }
        }

        public abstract void OnDeactivate();

        public void Deactivate()
        {
            OnDeactivate();

            // Deactivate the Commands
            foreach (AdnCommand command in commands)
            {
                command.Remove();
            }

            // Release objects.
            Marshal.ReleaseComObject(invApp);
            invApp = null;

            GC.WaitForPendingFinalizers();
            GC.Collect();
        }


        #region Obsolete interface methods

        [Obsolete("You should use the ControlDefinition functionality for implementing commands.", true)]
        public void ExecuteCommand(int commandId) { }

        public object Automation => null;

        #endregion
    }
}
