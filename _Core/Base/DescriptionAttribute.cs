﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common._Core.Base
{
    internal class DescriptionAttribute : Attribute
    {
        internal string Description;

        internal DescriptionAttribute(string description)
        {
            Description = description;
        }

        public override bool Equals(object obj)
        {
            if (obj as DescriptionAttribute == null) return false;
            return Description == ((DescriptionAttribute)obj).Description;
        }

        public override int GetHashCode()
        {
            return Description.GetHashCode();
        }
    }
}
