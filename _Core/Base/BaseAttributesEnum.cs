﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common._Core.Base
{
    public static class BaseAttributesEnum<T> // where T : Enum
    {
        public static Tt GetAttribute<Tt>(T tEnum) where Tt : Attribute
        {
            return (Tt)Attribute.GetCustomAttribute(ForValue(tEnum), typeof(Tt));
        }

        private static MemberInfo ForValue(T tEnum)
        {
            return typeof(T).GetField(Enum.GetName(typeof(T), tEnum));
        }

        public static T FindWithAttribute<Tt>(Tt attr, T tDefault) where Tt : Attribute
        {
            foreach (T tEnum in Enum.GetValues(typeof(T)).Cast<T>())
            {
                if (GetAttribute<Tt>(tEnum) == attr)
                {
                    return tEnum;
                }
            }
            return tDefault;
        }

        public static IEnumerable<T> FindAllWithAttribute<Tt>(Tt attr) where Tt : Attribute
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Where(tEnum => GetAttribute<Tt>(tEnum) == attr);
        }
    }
}