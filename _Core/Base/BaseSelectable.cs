﻿#region

using Inv.Common._Core.Interface;
using System.Collections.Generic;

#endregion

namespace Inv.Common._Core.Base
{
    public abstract class BaseSelectable<T> : ISelectable
    {
        protected BaseSelectable(T item)
        {
            Item = item;
        }

        protected BaseSelectable(T item, bool isSelected)
        {
            Item = item;
            IsSelected = isSelected;
        }

        public bool IsSelected { get; set; }
        public T Item { get; }

        protected bool Equals(BaseSelectable<T> other)
        {
            return EqualityComparer<T>.Default.Equals(Item, other.Item);
        }

        public override int GetHashCode()
        {
            return EqualityComparer<T>.Default.GetHashCode(Item);
        }
    }

    public class Selectable : BaseSelectable<object>
    {
        public Selectable(object o) : base(o) {}
    }
}