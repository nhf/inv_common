﻿#region

using Inv.Common.Adapter;
using Inventor;

#endregion

namespace Inv.Common._Core.Extensions
{
    public static class InventorExtensions
    {
        public const string AssemblyExt = ".iam";
        public const string PartExt = ".ipt";

        #region Assembly meta

        public static Matrix IdentityMatrix(this Application inventor)
        {
            return inventor.TransientGeometry.CreateMatrix();
        }

        #endregion

        #region Document Creation

        public static PartDocument CreatePart(this Application inventor, string fullFileName)
        {
            if (FileUtil.TryValidatePath(PartExt, fullFileName, out fullFileName))
            {
                return (PartDocument) CreateDocument(inventor, DocumentTypeEnum.kPartDocumentObject, fullFileName);
            }
            return null;
        }

        public static AssemblyDocument CreateAssembly(this Application inventor, string fullFileName)
        {
            if (FileUtil.TryValidatePath(AssemblyExt, fullFileName, out fullFileName))
            {
                return
                    (AssemblyDocument) CreateDocument(inventor, DocumentTypeEnum.kAssemblyDocumentObject, fullFileName);
            }
            return null;
        }

        private static Document CreateDocument(this Application inventor, DocumentTypeEnum docType, string fullPath)
        {
            var doc = inventor.Documents.Add
                (
                    docType,
                    inventor.FileManager.GetTemplateFile(docType),
                    false
                );
            doc.FullFileName = fullPath;
            doc.Save();
            return doc;
        }

        #endregion

        #region Document Opening/Saving

        public static Document LoadDocument(this Application inventor, string fullPath, bool visible = false)
        {
            if (string.IsNullOrEmpty(fullPath)) return null;
            return inventor.Documents.Open(fullPath, visible);
        }

        public static Document OpenDocument(Application inventor, string fullPath)
        {
            return LoadDocument(inventor, fullPath, true);
        }

        public static bool TryLoadDocument(this Application inventor, string fullPath, out Document doc, bool visible=false)
        {
            try
            {
                doc = LoadDocument(inventor, fullPath, visible);
            }
            catch
            {
                doc = null;
            }
            return doc != null;
        }

        public static ApprenticeServerDocument LoadDocument(this ApprenticeServerComponent apprentice, string fullPath)
        {
            if (string.IsNullOrEmpty(fullPath)) return null;
            return apprentice.Open(fullPath);
        }

        public static bool TryLoadDocument(this ApprenticeServerComponent apprentice, string fullPath,
            out ApprenticeServerDocument doc)
        {
            try
            {
                doc = LoadDocument(apprentice, fullPath);
            }
            catch
            {
                doc = null;
            }
            return doc != null;
        }

        public static void SaveDocumentAs(this Application inventor, string sourceFullPath, string newFullPath,
            bool silent = true)
        {
            Document doc;
            if (!TryLoadDocument(inventor, sourceFullPath, out doc)) return;
            doc.SaveAs(inventor, newFullPath, silent);
        }

        public static void SaveDocumentAs(this ApprenticeServerComponent apprentice, string sourceFullPath, string newFullPath)
        {
            ApprenticeServerDocument doc;
            if (!TryLoadDocument(apprentice, sourceFullPath, out doc)) return;
            doc.SaveAs(apprentice, newFullPath);
        }

        #endregion
    }
}