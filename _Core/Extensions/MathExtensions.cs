﻿#region

using System;

#endregion

namespace Inv.Common._Core.Extensions
{
    public static class MathExtensions
    {
        /// <summary>
        ///     Returns whether two numbers are equivalent within the specified precision.
        ///     Use this instead of .Equals or == when an approximate equivalence is good enough.
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <param name="precision"></param>
        /// <returns></returns>
        public static bool Approximately(this double d1, double d2, double precision = 3)
        {
            return Math.Abs(d1 - d2) < 1/Math.Pow(10, precision);
        }

        /// <summary>
        ///     Returns whether two numbers are equivalent within the specified precision.
        ///     Use this instead of .Equals or == when an approximate equivalence is good enough.
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="other">Must be a numerical type.</param>
        /// <param name="precision"></param>
        /// <returns></returns>
        public static bool Approximately(this double d1, object other, double precision = 3)
        {
            return Approximately(d1, Convert.ToDouble(other), precision);
        }
    }
}