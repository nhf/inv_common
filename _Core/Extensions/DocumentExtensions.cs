﻿#region

using Inv.Common.Adapter.InventorAdapters;
using Inventor;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

#endregion

// ReSharper disable SuspiciousTypeConversion.Global

namespace Inv.Common._Core.Extensions
{
    public static class DocumentExtensions
    {
        #region ComponentDefinition(s)

        public static ComponentDefinition GetComponentDefinition(this PartDocument doc)
        {
            return (ComponentDefinition)doc.ComponentDefinition;
        }

        public static ComponentDefinition GetComponentDefinition(this AssemblyDocument doc)
        {
            return (ComponentDefinition)doc.ComponentDefinition;
        }

        public static ComponentDefinition GetComponentDefinition<T>(this T doc) where T : Document
        {
            if (doc.IsPart())
            {
                return ((PartDocument)doc).GetComponentDefinition();
            }
            if (doc.IsAssembly())
            {
                return ((AssemblyDocument)doc).GetComponentDefinition();
            }
            return null;
        }

        #endregion

        #region Saving

        public static void SaveAs(this Document source, Application inventor, string newFullPath, bool silent = true,
            bool isDerived = true)
        {
            // Save the silent operation status to set back after the save op
            bool invSilentStatus = inventor.SilentOperation;

            // Set the silent op status for this operation
            inventor.SilentOperation = silent;

            // Execute the save
            source.SaveAs(newFullPath, isDerived);

            // Set the silent status back
            inventor.SilentOperation = invSilentStatus;
        }

        public static void SaveAs(this ApprenticeServerDocument source, ApprenticeServerComponent apprentice, string newFullPath, bool isDerived = true)
        {
            FileSaveAs save = apprentice.FileSaveAs;
            save.AddFileToSave(source, newFullPath);
            if (isDerived)
            {
                save.ExecuteSaveCopyAs();
                return;
            }
            save.ExecuteSaveAs();
        }

        #endregion

        #region Parameters

        /// <summary>
        /// Purges unused numerical parameters from the UserParameters. Leaves text units because
        /// they aren't necessarily used and are probably for categorization.
        /// </summary>
        /// <param name="doc"></param>
        public static void PurgeParameters(this Document doc)
        {
            /*
             * TODO: This exception list is a hack. Inventor API needs to update so that param Units are
             * directly accessible or Text/Boolean support is added in the UnitOfMeasure.GetTypeFromString() method
             */
            string[] unitTypeExceptions = { "Text", "Boolean" }; // Save non-dimensional params because they're metadata.

            UserParameterGroupWrapper userParams = new UserParameterGroupWrapper(doc);

            if (userParams.Contains("NO_PURGE")) return;
            IList<UserParameter> paramsDump = userParams.Items.Values.Select(param => (UserParameter)param).ToList();

            foreach (UserParameter unusedParam in paramsDump.Where(param => !param.InUse && !param.ExposedAsProperty))
            {
                try
                {
                    // Because the Units property is not directly accesible, this can cause problems...
                    string paramIntermediate = unusedParam.get_Units();
                    if (unitTypeExceptions.Contains(paramIntermediate)) continue;
                    userParams.Delete(unusedParam.Name);
                }
                catch
                {
                    // Skip the bad param
                }
            }
        }

        #endregion

        #region Document Constants

        private const string DocsubtypeSheetmetal = "{9C464203-9BAE-11D3-8BAD-0060B0CE6BB4}";
        public const string DocsubtypeStdpart = "{4D29B490-49B2-11D0-93C3-7E0706000000}";

        public const string X = "_X";
        public const string Y = "_Y";
        public const string Z = "_Z";

        #endregion

        #region DocumentType

        public static bool IsAssembly(this Document doc)
        {
            return doc.DocumentType == DocumentTypeEnum.kAssemblyDocumentObject;
        }

        public static bool IsAssembly(this ApprenticeServerDocument doc)
        {
            return doc.DocumentType == DocumentTypeEnum.kAssemblyDocumentObject;
        }

        public static bool IsPart(this Document doc)
        {
            return doc.DocumentType == DocumentTypeEnum.kPartDocumentObject;
        }

        public static bool IsSheetMetal(this PartDocument doc)
        {
            return ((Document)doc).IsSheetMetal();
        }

        public static bool IsSheetMetal(this Document doc)
        {
            return doc.SubType == DocsubtypeSheetmetal;
        }

        #endregion

        #region Geometry

        public static void AutoDim<T>(this T doc, string[] xyz = null, bool unfold = false) where T : Document
        {
            decimal[] dims = doc.Dimensions(unfold: unfold);
            var props = new PropertySetAdapter(doc, PropertySetAdapter.PropertysetsCustom)
            {
                [xyz?[0] ?? X] = dims[0],
                [xyz?[1] ?? Y] = dims[1],
                [xyz?[2] ?? Z] = dims[2]
            };
        }

        public static decimal[] Dimensions<T>(this T doc, int precision = 3, bool unfold = true) where T : Document
        {
            double[,] minMax = null;
            if (doc.IsPart())
            {
                minMax = PartDimensions((PartDocument) doc, unfold);
            }
            else if (doc.IsAssembly())
            {
                minMax = AsmDimensions((AssemblyDocument)doc);
            }
            if (minMax == null) return new decimal[] { -1, -1, -1 };

            decimal[] dims = { 0, 0, 0 };
            dims[0] = decimal.Round((minMax[1, 0] - minMax[0, 0]).ToDisplayUnits(doc), precision);
            dims[1] = decimal.Round((minMax[1, 1] - minMax[0, 1]).ToDisplayUnits(doc), precision);
            dims[2] = decimal.Round((minMax[1, 2] - minMax[0, 2]).ToDisplayUnits(doc), precision);

            return dims;
        }

        private static double[,] PartDimensions(PartDocument part, bool unfold)
        {
            // Returns the dimensions of the solid body(s) in the passed document A 1D, 3 element
            // decimal[] response is guaranteed

            if (!part.IsSheetMetal()) return BoxDimensions(part);

            SheetMetalComponentDefinition sheetMetalComponentDef = (SheetMetalComponentDefinition)part.ComponentDefinition;
            if (!sheetMetalComponentDef.HasFlatPattern)
            {
                sheetMetalComponentDef.Unfold();
            }

            return BoxDimensions(part);
        }

        private static double[,] AsmDimensions(AssemblyDocument asm)
        {
            double[,] dims = null;

            foreach (ComponentOccurrence occ in asm.ComponentDefinition.Occurrences)
            {
                if (!occ.Visible) continue;

                Document doc = (Document)occ.Definition.Document;

                double[,] localDims = null;

                if (doc.IsPart())
                {
                    localDims = BoxDimensions(occ);
                }

                if (localDims == null) continue;

                if (dims == null)
                {
                    dims = localDims;
                    continue;
                }
                else
                {
                    if (localDims[0, 0] < dims[0, 0]) dims[0, 0] = localDims[0, 0];
                    if (localDims[0, 1] < dims[0, 1]) dims[0, 1] = localDims[0, 1];
                    if (localDims[0, 2] < dims[0, 2]) dims[0, 2] = localDims[0, 2];
                    if (localDims[1, 0] > dims[1, 0]) dims[1, 0] = localDims[1, 0];
                    if (localDims[1, 1] > dims[1, 1]) dims[1, 1] = localDims[1, 1];
                    if (localDims[1, 2] > dims[1, 2]) dims[1, 2] = localDims[1, 2];
                }
            }
            return dims;
        }


        /// <summary>
        /// Returns the { { min }, { max } } dimensions of the part.
        /// </summary>
        /// <param name="componentDef"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        private static double[,] BoxDimensions(ComponentOccurrence occ)
        {
            Box rangeBox = occ.RangeBox;

            if (rangeBox == null) return new double[,] { { 0, 0, 0 }, { 0, 0, 0 } };

            return new double[,]
            {
                {
                    rangeBox.MinPoint.X,
                    rangeBox.MinPoint.Y,
                    rangeBox.MinPoint.Z
                },
                {
                    rangeBox.MaxPoint.X,
                    rangeBox.MaxPoint.Y,
                    rangeBox.MaxPoint.Z
                }
            };
        }

        private static double[,] BoxDimensions(PartDocument doc)
        {
            ComponentDefinition componentDef = doc.GetComponentDefinition();

            Box rangeBox = null;

            foreach (SurfaceBody surfaceBody in componentDef?.SurfaceBodies)
            {
                if (rangeBox == null)
                {
                    rangeBox = surfaceBody.RangeBox.Copy();
                }
                else
                {
                    rangeBox.Extend(surfaceBody.RangeBox.MinPoint);
                    rangeBox.Extend(surfaceBody.RangeBox.MaxPoint);
                }
            }

            if (rangeBox == null) return new double[,] { { 0, 0, 0 }, { 0, 0, 0 } };

            return new double[,]
            {
                {
                    rangeBox.MinPoint.X,
                    rangeBox.MinPoint.Y,
                    rangeBox.MinPoint.Z
                },
                {
                    rangeBox.MaxPoint.X,
                    rangeBox.MaxPoint.Y,
                    rangeBox.MaxPoint.Z
                }
            };
        }

        public static decimal ToDisplayUnits(this double d, Document source)
        {
            return (decimal) source.UnitsOfMeasure.ConvertUnits(d, UnitsTypeEnum.kDatabaseLengthUnits, UnitsTypeEnum.kInchLengthUnits);
        }

        public static decimal ToDisplayUnits(this double d, PartDocument source)
        {
            return d.ToDisplayUnits((Document)source);
        }

        #endregion

        #region Assemblies/Assembling

        public static IEnumerable<Document> GetComponentDocuments(this AssemblyDocument assembly)
        {
            IList<Document> componentDocuments = new List<Document>();
            foreach (ComponentOccurrence occ in assembly.ComponentDefinition.Occurrences)
            {
                componentDocuments.Add((Document)occ.Definition.Document);
            }
            return componentDocuments;
        }


        public static void InsertComponent(this AssemblyDocument assembly, Document component, Matrix position)
        {
            AssemblyComponentDefinition components = assembly.ComponentDefinition;
            components.Occurrences.Add(component.FullDocumentName, position);
        }

        public static void ReplaceComponent(this AssemblyDocument assembly, string originalFullPath,
            string replacementFullPath)
        {
            ReplaceComponent(assembly.ComponentDefinition, originalFullPath, replacementFullPath);
        }

        public static void ReplaceComponent(this AssemblyDocument assembly, Document original, Document replacement)
        {
            ReplaceComponent(assembly.ComponentDefinition, original.FullFileName, replacement.FullFileName);
        }

        public static void ReplaceComponent(this AssemblyComponentDefinition cd, string originalFullPath,
            string replacementFullPath)
        {
            foreach (
                ComponentOccurrence occ in
                    cd.Occurrences.Cast<ComponentOccurrence>()
                        .Where(occ => ((Document)occ.Definition.Document).FullFileName == originalFullPath))
            {
                ReplaceComponent(occ, replacementFullPath);
                break;
            }
        }

        private static void ReplaceComponent(ComponentOccurrence original, string replacementFullPath)
        {
            original.Replace(replacementFullPath, false);
        }

        public static void RemoveAllComponents(this AssemblyDocument assembly)
        {
            AssemblyComponentDefinition cd = assembly.ComponentDefinition;

            // It's a bad idea to directly modify a collection during iteration (here, deleting in a
            // loop could cause major problems) so dump the collection into a List and call Delete()
            // on each item of the List.

            List<ComponentOccurrence> occList = new List<ComponentOccurrence>(cd.Occurrences.Count);
            occList.AddRange(cd.Occurrences.Cast<ComponentOccurrence>());
            foreach (ComponentOccurrence occ in occList)
            {
                occ.Delete();
            }
        }

        public static bool ContainsDocument(this AssemblyDocument assembly, Document component)
        {
            return assembly.GetComponentDocuments().Select(doc => doc.FullFileName).Contains(component.FullFileName);
        }

        #endregion
    }
}