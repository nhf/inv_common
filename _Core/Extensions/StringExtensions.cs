﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common._Core.Extensions
{
    public static class StringExtensions
    {
        
        public static string RemoveAll(this string str, params string[] substrs)
        {
            if (string.IsNullOrEmpty(str)) return str;
            foreach (string substr in substrs)
            {
                int offset = 0;
                foreach (int index in str.AllIndexesOf(substr))
                {
                    str = str.Remove(index - offset, substr.Length);
                    offset += substr.Length;
                }
            }
            return str;
        }


        public static IEnumerable<int> AllIndexesOf(this string str, string substr)
        {
            if (string.IsNullOrEmpty(substr)) yield break;
            if (string.IsNullOrEmpty(str)) yield break;
            for (int index = 0; ; index += substr.Length)
            {
                index = str.IndexOf(substr, index);
                if (index == -1)
                    break;
                yield return index;
            }
        }

        public static bool TrySubstring(this string str, int start, int length, out string substr)
        {
            substr = "";
            if (str.Length < start + length) return false;
            if (start < 0 || length < 0) return false;
            substr = str.Substring(start, length);
            return true;
        }

        /// <summary>
        ///     Tests if a string contains any of the passed list of substrings.
        /// </summary>
        /// <param name="str">The string to check</param>
        /// <param name="arr">The list of substrings to test</param>
        /// <returns>The element of the substring array that the test string contains, if any; otherwise, an empty string.</returns>
        public static string Contains(this string str, IEnumerable<string> arr)
        {
            foreach (string s in arr)
            {
                if (str.Contains(s))
                {
                    return s;
                }
            }
            return "";
        }
    }
}
