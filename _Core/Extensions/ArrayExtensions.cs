﻿#region

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Inv.Common._Core.Extensions
{
    public static class ArrayExtensions
    {
        /// <summary>
        ///     Returns an array of length (n-1) with the element at i removed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static T[] Remove<T>(this T[] source, int index)
        {
            T[] dest = new T[source.Length - 1];
            if (index > 0)
                Array.Copy(source, 0, dest, 0, index);

            if (index < source.Length - 1)
                Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

            return dest;
        }

        /// <summary>
        ///     Returns an array of length (n+1) with the passed element appended at the end.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static T[] Append<T>(this T[] source, T t)
        {
            T[] dest = new T[source.Length + 1];
            Array.Copy(source, dest, source.Length);
            dest[dest.Length - 1] = t;
            return dest;
        }


        /// <summary>
        ///     Takes a collection of used ints and returns the lowest unused value.
        /// </summary>
        /// <param name="seq">An array of used ints.</param>
        /// <returns>The lowest unused int.</returns>
        public static int LowestUnused(this IEnumerable<int> seq)
        {
            List<int> sorted = seq.OrderBy(e => e).ToList();

            // Assuming we've numbered using the whole-number space, the first EngCode should be 1. We need to offset the 
            // index comparison since arrays index from 0.
            int offset = 1;

            if (sorted.First() == 0)
            {
                // If for some reason we've numbered using intspace and the first val is zero, it will line up exactly with
                // the array indices. No offsetting required so...
                offset = 0;
            }

            int i;
            if (sorted.Last() == sorted.Count() - offset)
            {
                i = sorted.Count() + 1;
            }
            else
            {
                // Run through the array comparing the index with the value to find the first mismatch
                for (i = offset; i < sorted.Count() + offset; i++)
                {
                    if (i != sorted[i - offset])
                    {
                        break;
                    }
                }
            }

            return i;
        }
    }
}