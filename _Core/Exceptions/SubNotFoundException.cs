﻿namespace Inv.Common._Core.Exceptions
{
    public class SubNotFoundException : LoggableException
    {
        public SubNotFoundException(string sub)
            : base("No Sub found: AsmProject.Module1." + sub + ".(PartDocument part)")
        {
        }
    }
}