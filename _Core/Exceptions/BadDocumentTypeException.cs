﻿#region

using Inventor;

#endregion

namespace Inv.Common._Core.Exceptions
{
    public class BadDocumentTypeException : LoggableException
    {
        public BadDocumentTypeException(DocumentTypeEnum expected, DocumentTypeEnum actual,
            bool autolog = false)
            : base("Expected Document Type: " + expected + "; Actual Document Type: " + actual, autolog)
        {
        }
    }
}