﻿#region

using System.Diagnostics;

#endregion

namespace Inv.Common._Core.Exceptions
{
    public class LoggableException : System.Exception
    {
        private readonly string _msg;

        protected LoggableException(string msg, bool autolog = false)
        {
            _msg = msg;
            if (autolog)
            {
                Log();
            }
        }

        protected LoggableException()
        {
        }

        public override string ToString()
        {
            return _msg;
        }

        private void Log()
        {
            Debug.Write(ToString());
        }
    }
}