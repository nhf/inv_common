﻿using Inv.Common._Core;
using Inv.Common._Core.Extensions;
using Inv.Common.Adapter;
using Inv.Common.Adapter.InventorAdapters;
using Inventor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Property = Inv.Common.R.Property;
using Path = System.IO.Path;

namespace Nhf.Inv.Common._Core
{
    public class PropertyValidator
    {
        public static List<string> ValidateDimensions(Document doc, bool unfoldSheet = false, IDataSet customProps = null)
        {
            if (customProps == null)
            {
                customProps = new PropertySetAdapter(doc, PropertySetAdapter.PropertysetsCustom);
            }

            List<string> errors = new List<string>();

            // Validate Length/Width/Height 
            decimal[] dims = doc.Dimensions(unfold: unfoldSheet);

            if (!(customProps.Contains(Property.KeyLength)
                  || customProps.Contains(Property.KeyHeight)
                  || customProps.Contains(Property.KeyWidth)))
            {
                errors.Add(doc.DisplayName + ": No dimensional iProperties found.");
            }
            else
            {
                foreach (string dimKey in new[] { Property.KeyLength, Property.KeyHeight, Property.KeyWidth })
                {
                    if (customProps.Contains(dimKey))
                    {
                        bool dimValid;
                        try
                        {
                            double dimension = Convert.ToDouble(customProps[dimKey]);
                            dimValid = dims.Any(dim => Convert.ToDouble(dim).Approximately(dimension));
                        }
                        catch
                        {
                            dimValid = false;
                        }

                        if (!dimValid)
                        {
                            errors.Add(doc.DisplayName + ": The " + dimKey +
                                       " iProperty does not match the Part Geometry.");
                        }
                    }
                }
            }

            if (errors.Count > 0)
            {
                errors.Add("Measured [" + String.Join(",", Array.ConvertAll(dims, d => d.ToString())) + "]");
            }

            return errors;
        }

        public static List<string> Validate(Document doc, bool unfoldSheet = false, bool fixPartNum = true)
        {
            List<string> errors = new List<string>();
            try
            {
                NhfComponent nhfComponent = NhfComponent.From(doc);

                var customProps = new PropertySetAdapter(doc, PropertySetAdapter.PropertysetsCustom);

                // Validate Length of part
                if (doc.IsPart())
                {
                    // Try and get Class
                    ClassId classId;
                    if (!Enum.TryParse((string)customProps[Property.KeyClassId], out classId))
                    {
                        if (nhfComponent.ClassCode != ClassId.None)
                        {
                            customProps[Property.KeyClassId] = nhfComponent.ClassCode;
                            classId = nhfComponent.ClassCode;
                        }
                    }

                    if (classId == ClassId.None)
                    {
                        errors.Add(doc.DisplayName + " has no Class ID.");
                    }

                    // Validate Bar Num if EX
                    if (classId == ClassId.EX)
                    {
                        string barNum = (string)customProps[Property.KeyBarNumber];

                        // First check that there is a bar number
                        if (!string.IsNullOrEmpty(barNum))
                        {
                            // Next check that the bar profile is correct for the part
                            string barProfile = barNum.Substring(7, 3);
                            string partProfile = nhfComponent.ProfileCode.ToString("D" + 3);

                            if (barProfile != partProfile)
                            {
                                errors.Add(doc.DisplayName + ": Bar Profile does not match Part Profile.");
                            }

                            // Next check that the bar is long enough to cut the part
                            double partLength;
                            if (customProps.Contains(Property.KeyLength))
                            {
                                string len = Convert.ToString(customProps[Property.KeyLength]);
                                string[] tokens = len.Split(' ');
                                partLength = Convert.ToDouble(tokens[0]);
                                double barLength = Convert.ToDouble(barNum.Substring(14, 3));
                                if (partLength > barLength)
                                {
                                    errors.Add(doc.DisplayName + ": Part is longer than Bar!");
                                }
                            }
                        }
                        else
                        {
                            errors.Add(doc.DisplayName + " does not have a Bar Number!!");
                        }
                    }

                    errors.AddRange(ValidateDimensions(doc, unfoldSheet));
                }


                // Check the filename agains the partnumber
                string fullFileName = doc.FullFileName;
                string fileName = Path.GetFileNameWithoutExtension(fullFileName);

                IDataSet designProps = new PropertySetAdapter(doc, PropertySetAdapter.PropertysetsDesign);
                string partNum = (string)designProps[Property.KeyPartNum];

                if (fileName != partNum)
                {
                    if (fixPartNum)
                    {
                        designProps[Property.KeyPartNum] = fileName;
                        errors.Add(doc.DisplayName + ": Part Number did not match file - fixed.");
                    }
                    else
                    {
                        errors.Add(doc.DisplayName + ": Part Number does not match filename.");
                    }
                }

                return errors;
            }
            catch
            {
                errors.Add("Error validating iProperties of " + doc.DisplayName);
            }
            return errors;
        }
    }
}
