﻿using Inv.Common._Core.Base;
using Inv.Common._Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Inv.Common.R;

namespace Inv.Common._Core.Util
{
    public static class EnumAttrUtil
    {
        public static IEnumerable<string> GetNames<T>() where T : struct
        {
            return Enum.GetNames(typeof(T));
        }

        public static T GetEnumFromString<T>(string s, T defaultValue) where T : struct
        {
            T result = defaultValue;

            string match = s.Contains(GetNames<T>());
            if (!string.IsNullOrEmpty(match))
            {
                Enum.TryParse(match, out result);
            }

            return result;
        }

        public static bool IsValid<T>(string check) where T : struct
        {
            foreach (string s in GetNames<T>())
            {
                if (check == s) return true;
            }
            return false;
        }

        public static string Description<T>(T t) where T : struct
        {
            return GetAttr(t).Description;
        }

        private static DescriptionAttribute GetAttr<T>(T t) where T : struct
        {
            return (DescriptionAttribute)Attribute.GetCustomAttribute(ForValue(t), typeof(DescriptionAttribute));
        }

        private static MemberInfo ForValue<T>(T t) where T : struct
        {
            return typeof(T).GetField(Enum.GetName(typeof(T), t));
        }

        public static T GetEnumFromDescription<T>(string desc, T defaultValue) where T : struct
        {
            foreach (T t in Enum.GetValues(typeof(T)))
            {
                if (Description(t) == desc)
                {
                    return t;
                }
            }
            return defaultValue;
        }

        public static AnalysisCode GetEnumFromName(string value)
        {
            throw new NotImplementedException();
        }
    }
}
