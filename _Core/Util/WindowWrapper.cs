﻿#region

using System;
using System.Windows.Forms;

#endregion

namespace Inv.Common._Core
{
    public sealed class WindowWrapper : IWin32Window
    {
        private readonly IntPtr _handle;

        public WindowWrapper(int mainFrameHwnd)
        {
            _handle = new IntPtr(mainFrameHwnd);
        }

        public WindowWrapper(IntPtr handle)
        {
            _handle = handle;
        }


        IntPtr IWin32Window.Handle => _handle;
    }
}