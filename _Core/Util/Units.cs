﻿using Inv.Common._Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common._Core.Util
{
    public class Units
    {
        public static string StripUnits(string raw)
        {
            string[] strToRemove =
            {
                " in",
                " ft",
                " cm",
                " mm",
                " ul",
                " m",
                " deg",
                " rad",
            };

            return raw?.RemoveAll(strToRemove);
        }
    }
}
