﻿#region

using System.Reflection;
using Inventor;

#endregion

namespace Inv.Common._Core.Adn
{
    //////////////////////////////////////////////////////////////////////////////////////////////
    // Description: Utility Class to manipulate Inventor Attributes
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    public class AdnAttributeManager
    {
        public static bool SetAttribute(
            object obj,
            string setName,
            string attName,
            object value,
            bool createIfNotExist)
        {
            return SetAttribute(obj, setName, attName, value, createIfNotExist, false);
        }

        public static bool SetAttribute(
            object obj,
            string setName,
            string attName,
            object value,
            bool createIfNotExist,
            bool copyWithOwner)
        {
            try
            {
                ValueTypeEnum valueType;

                string typeStr = value.GetType().ToString();

                switch (typeStr)
                {
                    case "System.Byte[]":
                        valueType = ValueTypeEnum.kByteArrayType;
                        break;

                    case "System.Double":
                        valueType = ValueTypeEnum.kDoubleType;
                        break;

                    case "System.Int32":
                        valueType = ValueTypeEnum.kIntegerType;
                        break;

                    case "System.String":
                        valueType = ValueTypeEnum.kStringType;
                        break;

                    default:
                        return false;
                }

                AttributeSets attributeSets = obj.GetType().InvokeMember(
                    "AttributeSets",
                    BindingFlags.GetProperty,
                    null,
                    obj,
                    null,
                    null, null, null) as AttributeSets;

                if (!attributeSets.get_NameIsUsed(setName))
                {
                    if (!createIfNotExist)
                        return false;

                    attributeSets.Add(setName, copyWithOwner);
                }

                AttributeSet attributeSet = attributeSets[setName];

                if (!attributeSet.get_NameIsUsed(attName))
                {
                    if (!createIfNotExist)
                        return false;

                    attributeSet.Add(attName, valueType, value);
                    return true;
                }

                Attribute attribute = attributeSet[attName];

                attribute.Value = value;

                return true;
            }
            catch
            {
                //error... 
                return false;
            }
        }

        public static Attribute GetAttribute(
            object obj,
            string setName,
            string attName)
        {
            try
            {
                AttributeSets attributeSets = obj.GetType().InvokeMember(
                    "AttributeSets",
                    BindingFlags.GetProperty,
                    null,
                    obj,
                    null,
                    null, null, null) as AttributeSets;

                AttributeSet attributeSet = attributeSets[setName];

                Attribute attribute = attributeSet[attName];

                return attribute;
            }
            catch
            {
                //error... 
                return null;
            }
        }
    }
}