#region

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using Inventor;

#endregion

namespace Inv.Common._Core.Adn
{
    /////////////////////////////////////////////////////////////
    // Use: Exposes OnTerminate Event for InteractionManager.
    //
    /////////////////////////////////////////////////////////////
    public class OnTerminateEventArgs : EventArgs
    {
        public OnTerminateEventArgs(bool isCancelled)
        {
            IsCancelled = isCancelled;
        }

        public bool IsCancelled { private set; get; }
    }

    public delegate void OnTerminateHandler(
        object sender,
        OnTerminateEventArgs e);

    /////////////////////////////////////////////////////////////////
    // A wrapper class to handle Interaction Events
    //
    /////////////////////////////////////////////////////////////////
    public class AdnInteractionManager
    {
        private bool _AllowCleanup;
        /////////////////////////////////////////////////////////////
        // Members
        //
        /////////////////////////////////////////////////////////////
        private readonly Application _Application;

        private bool _isCancelled;

        private readonly List<ObjectTypeEnum> _PreSelectFilters;

        /////////////////////////////////////////////////////////////
        // Constructor
        //
        /////////////////////////////////////////////////////////////
        public AdnInteractionManager(Application Application)
        {
            _Application = Application;

            InteractionEvents = null;

            SelectedEntities = new List<object>();

            _PreSelectFilters = new List<ObjectTypeEnum>();
        }

        public InteractionEvents InteractionEvents { get; private set; }

        public SelectEvents SelectEvents { get; private set; }

        public MouseEvents MouseEvents { get; private set; }

        /////////////////////////////////////////////////////////////
        // Use: Returns list of currently selected entities
        //
        /////////////////////////////////////////////////////////////
        public List<object> SelectedEntities { get; }

        /////////////////////////////////////////////////////////////
        // use: Initializes event handlers
        //
        /////////////////////////////////////////////////////////////
        public void Initialize()
        {
            InteractionEvents =
                _Application.CommandManager.CreateInteractionEvents();

            SelectEvents = InteractionEvents.SelectEvents;
            MouseEvents = InteractionEvents.MouseEvents;

            SelectEvents.Enabled = true;
            SelectEvents.SingleSelectEnabled = false;

            SelectEvents.OnPreSelect +=
                SelectEvents_OnPreSelect;

            SelectEvents.OnSelect +=
                SelectEvents_OnSelect;

            SelectEvents.OnUnSelect +=
                SelectEvents_OnUnSelect;

            InteractionEvents.OnTerminate +=
                InteractionEvents_OnTerminate;
        }

        /////////////////////////////////////////////////////////////
        // Use: Add selection filter.
        //
        /////////////////////////////////////////////////////////////
        public void AddSelectionFilter(SelectionFilterEnum filter)
        {
            if (SelectEvents != null)
            {
                SelectEvents.AddSelectionFilter(filter);
            }
        }

        /////////////////////////////////////////////////////////////
        // Use: Add pre-selection filter.
        //
        /////////////////////////////////////////////////////////////
        public void AddPreSelectionFilter(
            ObjectTypeEnum preSelectFilter)
        {
            _PreSelectFilters.Add(preSelectFilter);
        }

        /////////////////////////////////////////////////////////////
        // Use: Clear all selection filters
        //
        /////////////////////////////////////////////////////////////
        public void ClearSelectionFilters()
        {
            _PreSelectFilters.Clear();

            if (SelectEvents != null)
            {
                SelectEvents.ClearSelectionFilter();
            }
        }

        /////////////////////////////////////////////////////////////
        // Use: Starts selection.
        //
        /////////////////////////////////////////////////////////////
        public void Start(string statusbartxt)
        {
            try
            {
                if (InteractionEvents != null)
                {
                    SelectedEntities.Clear();

                    InteractionEvents.StatusBarText = statusbartxt;

                    InteractionEvents.Start();

                    _AllowCleanup = true;

                    _isCancelled = true;
                }
            }
            catch
            {
            }
        }

        public void Stop()
        {
            _AllowCleanup = false;

            InteractionEvents.Stop();
        }

        /////////////////////////////////////////////////////////////
        // Use: OnPreSelect Handler.
        //
        /////////////////////////////////////////////////////////////
        private void SelectEvents_OnPreSelect(
            ref object PreSelectEntity,
            out bool DoHighlight,
            ref ObjectCollection MorePreSelectEntities,
            SelectionDeviceEnum SelectionDevice,
            Point ModelPosition,
            Point2d ViewPosition,
            View View)
        {
            if (_PreSelectFilters.Count != 0 && !_PreSelectFilters.Contains(
                GetInventorType(PreSelectEntity)))
            {
                DoHighlight = false;
                return;
            }

            DoHighlight = true;
        }

        /////////////////////////////////////////////////////////////
        // Use: OnSelect Handler.
        //
        /////////////////////////////////////////////////////////////
        private void SelectEvents_OnSelect(
            ObjectsEnumerator JustSelectedEntities,
            SelectionDeviceEnum SelectionDevice,
            Point ModelPosition,
            Point2d ViewPosition,
            View View)
        {
            if (JustSelectedEntities.Count != 0)
            {
                object selectedObj = JustSelectedEntities[1];

                SelectedEntities.Add(selectedObj);
            }
        }

        /////////////////////////////////////////////////////////////
        // Use: OnUnSelect Handler.
        //
        /////////////////////////////////////////////////////////////
        private void SelectEvents_OnUnSelect(
            ObjectsEnumerator UnSelectedEntities,
            SelectionDeviceEnum SelectionDevice,
            Point ModelPosition,
            Point2d ViewPosition,
            View View)
        {
            foreach (object unselectedObj in
                UnSelectedEntities)
            {
                if (SelectedEntities.Contains(unselectedObj))
                {
                    SelectedEntities.Remove(unselectedObj);
                }
            }
        }

        /////////////////////////////////////////////////////////////
        // Use: Performs interaction cleanup.
        //
        /////////////////////////////////////////////////////////////
        private void CleanUp()
        {
            if (InteractionEvents != null)
            {
                SelectEvents.OnPreSelect -=
                    SelectEvents_OnPreSelect;

                SelectEvents.OnSelect -=
                    SelectEvents_OnSelect;

                SelectEvents.OnUnSelect -=
                    SelectEvents_OnUnSelect;

                InteractionEvents.OnTerminate -=
                    InteractionEvents_OnTerminate;

                Marshal.ReleaseComObject(SelectEvents);
                SelectEvents = null;

                Marshal.ReleaseComObject(InteractionEvents);
                InteractionEvents = null;
            }
        }

        /////////////////////////////////////////////////////////////
        // Use: Cancel Interaction event.
        //
        /////////////////////////////////////////////////////////////
        public void Cancel()
        {
            if (InteractionEvents != null)
            {
                _AllowCleanup = true;

                _isCancelled = true;

                InteractionEvents.Stop();
            }
        }

        /////////////////////////////////////////////////////////////
        // Use: Terminates Interaction event.
        //
        /////////////////////////////////////////////////////////////
        public void Terminate()
        {
            if (InteractionEvents != null)
            {
                _AllowCleanup = true;

                _isCancelled = false;

                InteractionEvents.Stop();
            }
        }

        /////////////////////////////////////////////////////////////
        // Use: OnTerminate handler.
        //
        /////////////////////////////////////////////////////////////
        private void InteractionEvents_OnTerminate()
        {
            if (_AllowCleanup)
            {
                CleanUp();

                OnTerminate(new OnTerminateEventArgs(_isCancelled));
            }
        }

        private void OnTerminate(OnTerminateEventArgs e)
        {
            if (OnTerminateEvent != null)
                OnTerminateEvent(this, e);
        }

        public event OnTerminateHandler OnTerminateEvent;

        /////////////////////////////////////////////////////////////
        // Use: Late-binded method to retrieve ObjectType property
        //
        /////////////////////////////////////////////////////////////
        private static ObjectTypeEnum GetInventorType(object obj)
        {
            try
            {
                object objType = obj.GetType().InvokeMember(
                    "Type",
                    BindingFlags.GetProperty,
                    null,
                    obj,
                    null,
                    null, null, null);

                return (ObjectTypeEnum) objType;
            }
            catch
            {
                //error...
                return ObjectTypeEnum.kGenericObject;
            }
        }
    }
}