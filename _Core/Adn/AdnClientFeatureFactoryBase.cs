﻿#region

using System;
using System.Drawing;
using Inventor;
using IPictureDisp = stdole.IPictureDisp;

#endregion

namespace Inv.Common._Core.Adn
{
    //////////////////////////////////////////////////////////////////////////////////////////////
    // Description: 
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    public class OnFeatureDoubleClickEventArgs : EventArgs
    {
        public OnFeatureDoubleClickEventArgs(
            _Document DocumentObject,
            ClientFeature Feature)
        {
            HandlingCode = HandlingCodeEnum.kEventNotHandled;

            Document = DocumentObject;

            ClientFeature = Feature;
        }

        public HandlingCodeEnum HandlingCode { get; set; }

        public Document Document { get; internal set; }

        public ClientFeature ClientFeature { get; internal set; }
    }

    public delegate void OnFeatureDoubleClickHandler(object sender,
        OnFeatureDoubleClickEventArgs e);

    //////////////////////////////////////////////////////////////////////////////////////////////
    // Description: 
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    public class OnClientFeatureSolveEventArgs : EventArgs
    {
        public OnClientFeatureSolveEventArgs(
            _Document document,
            ClientFeature feature,
            EventTimingEnum beforeOrAfter)
        {
            HandlingCode = HandlingCodeEnum.kEventNotHandled;

            Document = document;

            ClientFeature = feature;

            BeforeOrAfter = beforeOrAfter;
        }

        public HandlingCodeEnum HandlingCode { get; set; }

        public Document Document { get; private set; }

        public ClientFeature ClientFeature { get; private set; }

        public EventTimingEnum BeforeOrAfter { get; private set; }
    }

    public delegate void OnClientFeatureSolveHandler(object sender,
        OnClientFeatureSolveEventArgs e);

    //////////////////////////////////////////////////////////////////////////////////////////////
    // Description: 
    //
    //////////////////////////////////////////////////////////////////////////////////////////////
    public abstract class AdnClientFeatureFactoryBase
    {
        private readonly ModelingEvents _modelingEvents;

        protected AdnClientFeatureFactoryBase(Application inv)
        {
            _modelingEvents = inv.ModelingEvents;

            _modelingEvents.OnClientFeatureDoubleClick +=
                OnClientFeatureDoubleClick;

            _modelingEvents.OnClientFeatureSolve +=
                OnClientFeatureSolve;
        }

        public abstract string ClientFeatureName { get; }

        public abstract Bitmap ClientFeatureIcon { get; }

        public abstract bool IsCustomSolved { get; }

        public abstract bool HighlightClientGraphicsWithFeature { get; }

        protected virtual string ClientFeatureId
        {
            get { return "." + ClientFeatureName; }
        }

        public abstract bool Edit(ClientFeature feature);

        public abstract void ExitEdit();

        private ClientNodeResource CreateBrowserNodeResource(
            Document document,
            Bitmap icon)
        {
            try
            {
                IPictureDisp pic = PictureDispConverter.ToIPictureDisp(icon);

                ClientNodeResource res = document.BrowserPanes.ClientNodeResources.Add(
                    ClientFeatureId,
                    document.BrowserPanes.ClientNodeResources.Count + 1,
                    pic);

                return res;
            }
            catch
            {
                return null;
            }
        }

        private ClientNodeResource GetBrowserNodeResource(Document document)
        {
            foreach (ClientNodeResource resource in document.BrowserPanes.ClientNodeResources)
            {
                if (resource.ClientId == ClientFeatureId)
                    return resource;
            }

            return CreateBrowserNodeResource(document, ClientFeatureIcon);
        }

        protected ClientFeature CreateClientFeature(
            Application inv,
            Document document,
            object startElement,
            object endElement,
            object inputs)
        {
            try
            {
                ComponentDefinition compDef =
                    AdnInventorUtilities.GetCompDefinition(document);

                object features =
                    AdnInventorUtilities.GetProperty(compDef, "Features");

                ClientFeatures clientFeatures =
                    AdnInventorUtilities.GetProperty(features, "ClientFeatures")
                        as ClientFeatures;

                ClientFeatureDefinition cfDef =
                    clientFeatures.CreateDefinition(
                        ClientFeatureName,
                        startElement,
                        endElement,
                        inputs);

                cfDef.HighlightClientGraphicsWithFeature =
                    HighlightClientGraphicsWithFeature;

                cfDef.IsCustomSolved = IsCustomSolved;

                ClientFeature clientFeature = clientFeatures.Add(
                    cfDef,
                    ClientFeatureId);

                NativeBrowserNodeDefinition nodeDef =
                    clientFeature.BrowserNode.BrowserNodeDefinition
                        as NativeBrowserNodeDefinition;

                nodeDef.OverrideIcon = GetBrowserNodeResource(document);

                return clientFeature;
            }
            catch
            {
                return null;
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////////
        // Description: 
        //
        //////////////////////////////////////////////////////////////////////////////////////////////
        private void OnFeatureDoubleClick(OnFeatureDoubleClickEventArgs e)
        {
            OnFeatureDoubleClickEvent?.Invoke(this, e);
        }

        public event OnFeatureDoubleClickHandler OnFeatureDoubleClickEvent;

        private void OnClientFeatureDoubleClick(
            _Document DocumentObject,
            ClientFeature Feature,
            NameValueMap Context,
            out HandlingCodeEnum HandlingCode)
        {
            HandlingCode = HandlingCodeEnum.kEventNotHandled;

            if (Feature.ClientId == ClientFeatureId)
            {
                OnFeatureDoubleClickEventArgs args =
                    new OnFeatureDoubleClickEventArgs(
                        DocumentObject,
                        Feature);

                OnFeatureDoubleClick(args);

                HandlingCode = args.HandlingCode;
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////////
        // Description: 
        //
        //////////////////////////////////////////////////////////////////////////////////////////////
        private void OnClientFeatureSolve(OnClientFeatureSolveEventArgs e)
        {
            OnClientFeatureSolveEvent?.Invoke(this, e);
        }

        public event OnClientFeatureSolveHandler OnClientFeatureSolveEvent;

        private void OnClientFeatureSolve(
            _Document DocumentObject,
            ClientFeature Feature,
            EventTimingEnum BeforeOrAfter,
            NameValueMap Context,
            out HandlingCodeEnum HandlingCode)
        {
            HandlingCode = HandlingCodeEnum.kEventNotHandled;

            if (Feature.ClientId == ClientFeatureId)
            {
                OnClientFeatureSolveEventArgs args =
                    new OnClientFeatureSolveEventArgs(
                        DocumentObject,
                        Feature,
                        BeforeOrAfter);

                OnClientFeatureSolve(args);

                HandlingCode = args.HandlingCode;
            }
        }
    }
}