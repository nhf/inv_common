﻿#region

using System;
using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using Inv.Common.Properties;
using Inventor;

#endregion

namespace Inv.Common._Core.Adn
{
    public abstract class AdnButtonCommandBase : AdnCommand
    {
        private readonly bool _bNotifyExecWhenRunning;

        private bool _bIsRunning;
        protected ButtonDefinition _controlDef;

        protected AdnButtonCommandBase(Inventor.Application application) :
            base(application)
        {
            _bIsRunning = false;

            _bNotifyExecWhenRunning = false;

            CommandWindow = null;

            InteractionManager = new AdnInteractionManager(application);

            InteractionManager.OnTerminateEvent += Handle_OnTerminateEvent;
        }

        public Window CommandWindow { get; internal set; }

        public bool DisplayFormModal { get; internal set; }

        public AdnInteractionManager InteractionManager { get; protected set; }

        protected override void CreateControl()
        {
            CommandManager commandMgr = InvApp.CommandManager;

            //Create IPictureDisp from Icon resources
            stdole.IPictureDisp standardIconPict = StandardIcon != null
                ? PictureDispConverter.ToIPictureDisp(StandardIcon)
                : null;
            stdole.IPictureDisp largeIconPict = LargeIcon != null ? PictureDispConverter.ToIPictureDisp(LargeIcon) : null;

            _controlDef = commandMgr.ControlDefinitions.AddButtonDefinition(
                DisplayName,
                InternalName,
                Classification,
                ClientId,
                Description,
                ToolTipText,
                StandardIcon: standardIconPict,
                LargeIcon: largeIconPict,
                ButtonDisplay: ButtonDisp);

            ControlDef = _controlDef as ControlDefinition;

            _controlDef.OnExecute += Handle_ButtonDefinition_OnExecute;
            _controlDef.OnHelp += Handle_ButtonDefinition_OnHelp;
        }

        private void OnCommandTerminatedOrCancelled()
        {
            _bIsRunning = false;

            _controlDef.Pressed = false;

            if (CommandWindow != null)
            {
                CommandWindow.Closed -= CommandWindow_WindowClosed;
                CommandWindow.Close();
            }
        }

        protected virtual void Cancel()
        {
            OnCommandTerminatedOrCancelled();
            InteractionManager.Cancel();
        }

        protected virtual void Terminate()
        {
            OnCommandTerminatedOrCancelled();
            InteractionManager.Terminate();
        }

        private void Handle_OnTerminateEvent(
            object o, OnTerminateEventArgs e)
        {
            OnCommandTerminatedOrCancelled();
        }

        protected abstract void OnExecute(NameValueMap context);

        protected abstract void OnHelp(NameValueMap context);

        private void Handle_ButtonDefinition_OnExecute(NameValueMap context)
        {
            try
            {
                if (!_bIsRunning)
                {
                    _bIsRunning = true;
                    _controlDef.Pressed = true;

                    if (InvApp.ActiveDocument != null)
                        InteractionManager.Initialize();

                    OnExecute(context);
                }
                else if (_bNotifyExecWhenRunning) OnExecute(context);

                DisplayWindow();
            }
            catch (System.Exception)
            {
                Terminate();
            }
        }

        private void Handle_ButtonDefinition_OnHelp(
            NameValueMap context,
            out HandlingCodeEnum handlingCode)
        {
            handlingCode = HandlingCodeEnum.kEventHandled;

            try
            {
                OnHelp(context);
            }
            catch
            {
                // ignored
            }
        }

        #region IconMembers

        protected virtual Icon StandardIcon => new Icon(Resources.ic_device_hub_black, 16, 16);

        protected virtual Icon LargeIcon => new Icon(Resources.ic_device_hub_black, 32, 32);

        #endregion

        #region Window Members

        protected void RegisterCommandWindow(Window win, bool modal)
        {
            CommandWindow = win;
            new WindowInteropHelper(win).Owner = new IntPtr(InvApp.MainFrameHWND);

            DisplayFormModal = modal;

            CommandWindow.Closed += CommandWindow_WindowClosed;
            /*
            CommandWindow.KeyPreview = true;

            CommandWindow.FormClosed +=
                new FormClosedEventHandler(CommandMainForm_FormClosed);

            CommandWindow.KeyDown +=
                new KeyEventHandler(CommandMainForm_KeyDown);
            */
        }

        private void DisplayWindow()
        {
            if (CommandWindow == null) return;
            if (DisplayFormModal)
            {
                CommandWindow.ShowDialog();
            }
            else
            {
                CommandWindow.Show();
            }
        }

        private void CommandWindow_WindowClosed(object sender, EventArgs e)
        {
            if (CommandWindow.DialogResult ?? false)
            {
                // Do something with OK result 
                // Terminate();
            }
            OnCommandTerminatedOrCancelled();
        }

        /*

        private void CommandMainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                if (CommandWindow.DialogResult != DialogResult.Ignore)
                    CommandWindow.Close();
            }
        }
        */

        #endregion
    }
}