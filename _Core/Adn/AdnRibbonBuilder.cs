#region

using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Xml;
using Inventor;

#endregion

namespace Inv.Common._Core.Adn
{
    public class AdnRibbonBuilder
    {
        private readonly string _addinGuid;
        private readonly Application _application;

        private AdnRibbonBuilder(Application application, string guid)
        {
            _application = application;
            _addinGuid = guid;
        }

        // Create Ribbon items based on a Stream generated from an XML description file 
        public static void CreateRibbon(Application app, Type addinType, Stream stream)
        {
            new AdnRibbonBuilder(app, addinType.GUID.ToString("B")).DoCreateRibbon(stream);
        }

        // Create Ribbon items based on a Stream name, typically an embedded resource xml file
        public static void CreateRibbon(Application app, Type addinType, string resourcename)
        {
            Stream xmlres = addinType.Assembly.GetManifestResourceStream(resourcename);
            new AdnRibbonBuilder(app, addinType.GUID.ToString("B")).DoCreateRibbon(xmlres);
        }

        // Create Ribbon items based on an existing xml file on the disk
        public static void CreateRibbonFromFile(Application app, Type addinType, string xmlfile)
        {
            new AdnRibbonBuilder(app, addinType.GUID.ToString("B")).DoCreateRibbon(xmlfile);
        }

        private void DoCreateRibbon(Stream stream)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(stream);
            ProcessDocument(xdoc);
        }

        private void DoCreateRibbon(string fileName)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(fileName);
            ProcessDocument(xdoc);
        }

        private void ProcessDocument(XmlDocument xdoc)
        {
            XmlNodeList tabNodes = xdoc.SelectNodes("//RibbonTab");

            if (tabNodes == null) return;
            foreach (XmlNode tabNode in tabNodes)
            {
                ProcessTabNode(tabNode);
            }
        }

        private void ProcessTabNode(XmlNode node)
        {
            UserInterfaceManager uiManager = _application.UserInterfaceManager;

            string[] ribbonNames = node.Attributes?["ribbons"].Value.Split('|');
            string displayName = XmlUtilities.GetAttributeValue<string>(node, "displayName");
            string internalName = node.Attributes?["internalName"].Value;
            string targetTab = XmlUtilities.GetAttributeValue<string>(node, "targetTab");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? contextual = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "contextual");

            foreach (Ribbon ribbon in uiManager.Ribbons)
            {
                if (ribbonNames != null && ribbonNames.Contains(ribbon.InternalName) == false)
                {
                    continue;
                }

                RibbonTab tab =
                    ribbon.RibbonTabs.OfType<RibbonTab>().SingleOrDefault(t => t.InternalName == internalName) ??
                    ribbon.RibbonTabs.Add(
                        displayName,
                        internalName,
                        _addinGuid,
                        string.IsNullOrEmpty(targetTab) ? string.Empty : targetTab,
                        insertBefore ?? false,
                        contextual ?? false);

                XmlNodeList panelNodes = node.SelectNodes("RibbonPanel");

                if (panelNodes == null) continue;
                foreach (XmlNode panelNode in panelNodes)
                {
                    ProcessPanelNode(tab, panelNode);
                }
            }
        }

        private void ProcessPanelNode(RibbonTab tab, XmlNode node)
        {
            string displayName = XmlUtilities.GetAttributeValue<string>(node, "displayName");
            string internalName = node.Attributes?["internalName"].Value;
            string targetPanel = XmlUtilities.GetAttributeValue<string>(node, "targetPanel");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");

            RibbonPanel panel =
                tab.RibbonPanels.OfType<RibbonPanel>().SingleOrDefault(p => p.InternalName == internalName);

            if (panel == null)
            {
                panel = tab.RibbonPanels.Add(
                    displayName,
                    internalName,
                    _addinGuid,
                    string.IsNullOrEmpty(targetPanel) ? string.Empty : targetPanel,
                    insertBefore.HasValue ? insertBefore.Value : false);
            }

            foreach (XmlNode childNode in node.ChildNodes)
            {
                Action<RibbonPanel, XmlNode> processMethod;

                switch (childNode.Name)
                {
                    case "Button":
                        processMethod = ProcessButtonNode;
                        break;

                    case "Separator":
                        processMethod = ProcessSeparatorNode;
                        break;

                    case "ButtonPopup":
                        processMethod = ProcessButtonPopupNode;
                        break;

                    case "Popup":
                        processMethod = ProcessPopupNode;
                        break;

                    case "ComboBox":
                        processMethod = ProcessComboBoxNode;
                        break;

                    case "Gallery":
                        processMethod = ProcessGalleryNode;
                        break;

                    case "Macro":
                        processMethod = ProcessMacroNode;
                        break;

                    case "SplitButton":
                        processMethod = ProcessSplitButtonNode;
                        break;

                    case "SplitButtonMRU":
                        processMethod = ProcessSplitButtonMruNode;
                        break;

                    case "TogglePopup":
                        processMethod = ProcessTogglePopupNode;
                        break;

                    default:
                        continue;
                }

                try
                {
                    processMethod.Invoke(panel, childNode);
                }
                catch
                {
                    // ignored
                }
            }
        }

        private void ProcessButtonNode(RibbonPanel panel, XmlNode node)
        {
            string commandName = node.Attributes?["internalName"].Value;

            ButtonDefinition controlDef =
                // ReSharper disable once SuspiciousTypeConversion.Global
                _application.CommandManager.ControlDefinitions[commandName] as ButtonDefinition;

            bool? useLargeIcon = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "useLargeIcon");
            bool? showText = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "showText");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            controls.AddButton(
                controlDef,
                useLargeIcon ?? false,
                showText ?? true,
                string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                insertBefore ?? false);
        }

        private void ProcessSeparatorNode(RibbonPanel panel, XmlNode node)
        {
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            controls.AddSeparator(
                string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                insertBefore ?? false);
        }

        private void ProcessButtonPopupNode(RibbonPanel panel, XmlNode node)
        {
            bool? useLargeIcon = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "useLargeIcon");
            bool? showText = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "showText");
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            ObjectCollection buttons = GetButtons(node);

            if (buttons.Count > 0)
            {
                controls.AddButtonPopup(
                    buttons,
                    useLargeIcon ?? false,
                    showText ?? true,
                    string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                    insertBefore.HasValue && insertBefore.Value);
            }
        }

        private void ProcessPopupNode(RibbonPanel panel, XmlNode node)
        {
            string commandName = node.Attributes?["internalName"].Value;

            // ReSharper disable once SuspiciousTypeConversion.Global
            ButtonDefinition controlDef =
                _application.CommandManager.ControlDefinitions[commandName] as ButtonDefinition;

            bool? useLargeIcon = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "useLargeIcon");
            bool? showText = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "showText");
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            ObjectCollection buttons = GetButtons(node);

            if (buttons.Count > 0)
            {
                controls.AddPopup(
                    controlDef,
                    buttons,
                    useLargeIcon ?? false,
                    showText ?? true,
                    string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                    insertBefore.HasValue && insertBefore.Value);
            }
        }

        private void ProcessComboBoxNode(RibbonPanel panel, XmlNode node)
        {
            string commandName = node.Attributes?["internalName"].Value;

            // ReSharper disable once SuspiciousTypeConversion.Global
            ComboBoxDefinition controlDef =
                _application.CommandManager.ControlDefinitions[commandName] as ComboBoxDefinition;

            //bool? useLargeIcon = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "useLargeIcon");
            //bool? showText = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "showText");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            controls.AddComboBox(
                controlDef,
                string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                insertBefore ?? false);

            XmlNodeList nodes = node.SelectNodes("ComboItem");

            if (nodes == null) return;
            foreach (XmlNode comboItemNode in nodes)
            {
                string comboItem = comboItemNode.Attributes?["item"].Value;

                int? index = XmlUtilities.GetAttributeValueAsNullable<int>(comboItemNode, "index");

                controlDef?.AddItem(
                    comboItem,
                    index ?? 0);
            }
        }

        private void ProcessGalleryNode(RibbonPanel panel, XmlNode node)
        {
            // string commandName = node.Attributes?["internalName"].Value;

            // ReSharper disable once SuspiciousTypeConversion.Global
            // ButtonDefinition controlDef = _application.CommandManager.ControlDefinitions[commandName] as ButtonDefinition;

            bool? displayAsCombo = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "displayAsCombo");
            int? width = XmlUtilities.GetAttributeValueAsNullable<int>(node, "width");
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            ObjectCollection buttons = GetButtons(node);

            if (buttons.Count > 0)
            {
                controls.AddGallery(
                    buttons,
                    displayAsCombo ?? false,
                    width ?? 15,
                    string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                    insertBefore ?? false);
            }
        }

        private void ProcessMacroNode(RibbonPanel panel, XmlNode node)
        {
            string commandName = node.Attributes?["internalName"].Value;

            // ReSharper disable once SuspiciousTypeConversion.Global
            MacroControlDefinition controlDef =
                _application.CommandManager.ControlDefinitions[commandName] as MacroControlDefinition;

            bool? useLargeIcon = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "useLargeIcon");
            bool? showText = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "showText");
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            controls.AddMacro(
                controlDef,
                useLargeIcon ?? false,
                showText ?? true,
                string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                insertBefore ?? false);
        }

        private void ProcessSplitButtonNode(RibbonPanel panel, XmlNode node)
        {
            string commandName = node.Attributes?["internalName"].Value;

            // ReSharper disable once SuspiciousTypeConversion.Global
            ButtonDefinition controlDef =
                _application.CommandManager.ControlDefinitions[commandName] as ButtonDefinition;

            bool? useLargeIcon = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "useLargeIcon");
            bool? showText = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "showText");
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            ObjectCollection buttons = GetButtons(node);

            if (buttons.Count > 0)
            {
                controls.AddSplitButton(
                    controlDef,
                    buttons,
                    useLargeIcon ?? false,
                    showText ?? true,
                    string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                    insertBefore ?? false);
            }
        }

        private void ProcessSplitButtonMruNode(RibbonPanel panel, XmlNode node)
        {
            bool? useLargeIcon = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "useLargeIcon");
            bool? showText = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "showText");
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            ObjectCollection buttons = GetButtons(node);

            if (buttons.Count > 0)
            {
                controls.AddSplitButtonMRU(
                    buttons,
                    useLargeIcon ?? false,
                    showText ?? true,
                    string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                    insertBefore ?? false);
            }
        }

        private void ProcessTogglePopupNode(RibbonPanel panel, XmlNode node)
        {
            string commandName = node.Attributes?["internalName"].Value;

            // ReSharper disable once SuspiciousTypeConversion.Global
            ButtonDefinition controlDef =
                _application.CommandManager.ControlDefinitions[commandName] as ButtonDefinition;

            bool? useLargeIcon = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "useLargeIcon");
            bool? showText = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "showText");
            string targetControl = XmlUtilities.GetAttributeValue<string>(node, "targetControl");
            bool? insertBefore = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "insertBefore");
            bool? isInSlideout = XmlUtilities.GetAttributeValueAsNullable<bool>(node, "isInSlideout");

            bool slideout = isInSlideout ?? false;

            CommandControls controls = slideout ? panel.SlideoutControls : panel.CommandControls;

            ObjectCollection buttons = GetButtons(node);

            if (buttons.Count > 0)
            {
                controls.AddTogglePopup(
                    controlDef,
                    buttons,
                    useLargeIcon ?? false,
                    showText ?? true,
                    string.IsNullOrEmpty(targetControl) ? string.Empty : targetControl,
                    insertBefore ?? false);
            }
        }

        private ObjectCollection GetButtons(XmlNode node)
        {
            ObjectCollection buttons = _application.TransientObjects.CreateObjectCollection(Type.Missing);

            XmlNodeList nodes = node.SelectNodes("Button");

            if (nodes == null) return buttons;
            // ReSharper disable once SuspiciousTypeConversion.Global
            foreach (ButtonDefinition buttonDef in 
                (from XmlNode buttonNode in nodes
                    select buttonNode.Attributes?["internalName"].Value
                    into commandName
                    select _application.CommandManager.ControlDefinitions[commandName]).OfType<ButtonDefinition>())
            {
                buttons.Add(buttonDef);
            }

            return buttons;
        }
    }

    // ReSharper disable once ClassNeverInstantiated.Global
    internal class XmlUtilities
    {
        public static T? GetAttributeValueAsNullable<T>(XmlNode node, string attributeName) where T : struct
        {
            XmlAttribute attr = node.Attributes?.GetNamedItem(attributeName) as XmlAttribute;

            if (attr == null) return null;
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

            object o = converter.ConvertFrom(attr.Value);
            return (T?) o;
        }

        public static T GetAttributeValue<T>(XmlNode node, string attributeName) where T : class
        {
            XmlAttribute attr = node.Attributes?.GetNamedItem(attributeName) as XmlAttribute;

            if (attr == null) return null;
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

            return (T) converter.ConvertFrom(attr.Value);
        }
    }
}