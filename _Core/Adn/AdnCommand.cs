﻿#region

using System.Reflection;
using Inventor;
using Inv.Common.Adapter.InventorAdapters;

#endregion

namespace Inv.Common._Core.Adn
{
    public abstract class AdnCommand
    {
        protected AdnCommand(Application invApp)
        {
            InvApp = invApp;

            UserInputEvents userInputEvents = InvApp.CommandManager.UserInputEvents;

            userInputEvents.OnLinearMarkingMenu += OnLinearMarkingMenu;

            userInputEvents.OnRadialMarkingMenu += OnRadialMarkingMenu;
        }

        protected Assembly ResAssembly { get; set; }

        public ControlDefinition ControlDef { get; internal set; }

        public readonly Application InvApp;

        public abstract string DisplayName { get; }

        public abstract string InternalName { get; }

        public abstract CommandTypesEnum Classification { get; }

        public virtual string ClientId { get; protected set; }

        public abstract string Description { get; }

        public abstract string ToolTipText { get; }

        public abstract ButtonDisplayEnum ButtonDisp { get; }

        // Static method to add new command, needs to be called
        // in add-in Activate typically
        public static void AddCommand(AdnCommand command)
        {
            command.ResAssembly = Assembly.GetCallingAssembly();

            command.CreateControl();
        }

        public static void AddCommand(AdnCommand command, Assembly resAssembly)
        {
            command.ResAssembly = resAssembly;

            command.CreateControl();
        }

        protected abstract void CreateControl();

        public void Remove()
        {
            OnRemove();
            ControlDef?.Delete();
        }

        protected virtual void OnRemove()
        {
        }

        protected virtual void OnLinearMarkingMenu(
            ObjectsEnumerator SelectedEntities,
            SelectionDeviceEnum SelectionDevice,
            CommandControls LinearMenu,
            NameValueMap AdditionalInfo)
        {
        }

        protected virtual void OnRadialMarkingMenu(
            ObjectsEnumerator SelectedEntities,
            SelectionDeviceEnum SelectionDevice,
            RadialMarkingMenu RadialMenu,
            NameValueMap AdditionalInfo)
        {
        }

        public static void Execute(Application invApp, string internalName, bool synchronous)
        {
            invApp?.CommandManager?.ControlDefinitions[internalName]?.Execute2(synchronous);
        }

        public static bool TryExecute(Application invApp, string internalName, bool synchronous)
        {
            try
            {
                Execute(invApp, internalName, synchronous);
                return true;
            }
            catch
            {
                // Squash
            }
            return false;
        }
    }
}