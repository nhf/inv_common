﻿#region

using System.Reflection;
using Inventor;

#endregion

namespace Inv.Common._Core.Adn
{
    public class AdnRefKey
    {
        internal byte[] _contextData;
        internal byte[] _refKey;

        internal AdnRefKey(byte[] refKey, byte[] contextData)
        {
            _refKey = refKey;
            _contextData = contextData;
        }

        public AdnRefKey() :
            this(new byte[] {}, new byte[] {})
        {
        }
    }

    public class AdnRefKeyManager
    {
        public static bool GetAdnReferenceKey(
            Document doc,
            object obj,
            ref AdnRefKey adnRefKey)
        {
            return GetReferenceKey(
                doc,
                obj,
                ref adnRefKey._refKey,
                ref adnRefKey._contextData);
        }

        public static object GetObject(
            Document doc,
            AdnRefKey adnRefKey)
        {
            SolutionNatureEnum matchType;

            return GetObject(doc, adnRefKey._refKey, adnRefKey._contextData, out matchType);
        }

        public static bool StoreRefObjectInAttribute(
            Document doc,
            object refObj,
            object targetObj,
            string setName,
            string attName)
        {
            AdnRefKey adnRefKey = new AdnRefKey();

            if (!GetAdnReferenceKey(doc, refObj, ref adnRefKey))
                return false;

            if (!AdnAttributeManager.SetAttribute(
                targetObj,
                setName,
                attName + ".Key",
                adnRefKey._refKey,
                true))
                return false;

            if (!AdnAttributeManager.SetAttribute(
                targetObj,
                setName,
                attName + ".Context",
                adnRefKey._contextData,
                true))
                return false;

            return true;
        }

        public static bool StoreRefKeyInAttribute(
            AdnRefKey adnRefKey,
            object targetObj,
            string setName,
            string attName)
        {
            bool res1 = AdnAttributeManager.SetAttribute(
                targetObj,
                setName,
                attName + ".Context",
                adnRefKey._contextData,
                true,
                false);

            bool res2 = AdnAttributeManager.SetAttribute(
                targetObj,
                setName,
                attName + ".Key",
                adnRefKey._refKey,
                true,
                false);

            return res1 && res2;
        }

        public static object GetRefObjectFromAttribute(
            Document doc,
            object targetObj,
            string setName,
            string attName)
        {
            try
            {
                Attribute attKey =
                    AdnAttributeManager.GetAttribute(
                        targetObj,
                        setName,
                        attName + ".Key");

                Attribute attContext =
                    AdnAttributeManager.GetAttribute(
                        targetObj,
                        setName,
                        attName + ".Context");

                AdnRefKey adnRefKey = new AdnRefKey(
                    (byte[]) attKey.Value,
                    (byte[]) attContext.Value);

                return GetObject(doc, adnRefKey);
            }
            catch
            {
                return null;
            }
        }

        public static bool GetReferenceKey(
            Document doc,
            object obj,
            ref byte[] refKey,
            ref byte[] contextData)
        {
            try
            {
                int keyContext = doc.ReferenceKeyManager.CreateKeyContext();

                object[] Params = new object[2];

                Params[0] = refKey;
                Params[1] = keyContext;

                // Set up the parameter modifiers.
                ParameterModifier[] mods = new ParameterModifier[2]
                {
                    new ParameterModifier(1),
                    new ParameterModifier(1)
                };

                // Set the first parameter to be allowed to be modified.
                mods[0][0] = true;

                obj.GetType().InvokeMember(
                    "GetReferenceKey",
                    BindingFlags.InvokeMethod,
                    null,
                    obj,
                    Params,
                    mods,
                    null,
                    null);

                refKey = (byte[]) Params[0];

                doc.ReferenceKeyManager.SaveContextToArray(keyContext, ref contextData);

                return true;
            }
            catch
            {
                //The object doesn't support ReferenceKey
                refKey = null;
                contextData = null;
                return false;
            }
        }


        public static object GetObject(
            Document doc,
            byte[] refKey,
            byte[] contextData)
        {
            SolutionNatureEnum matchType;

            return GetObject(doc, refKey, contextData, out matchType);
        }

        public static object GetObject(
            Document doc,
            byte[] refKey,
            byte[] contextData,
            out SolutionNatureEnum matchType)
        {
            try
            {
                int keyContext = doc.ReferenceKeyManager.LoadContextFromArray(ref contextData);

                object matchTypeObj;

                object obj = doc.ReferenceKeyManager.BindKeyToObject(
                    ref refKey,
                    keyContext,
                    out matchTypeObj);

                matchType = (SolutionNatureEnum) matchTypeObj;

                return obj;
            }
            catch
            {
                matchType = SolutionNatureEnum.kNoSolution;
                return null;
            }
        }
    }
}