#region

using System;
using System.Runtime.InteropServices;

#endregion

namespace Inv.Common._Core.Adn
{
    public class CommonUtils
    {
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetCurrentProcess();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr GetModuleHandle(string moduleName);

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetProcAddress(IntPtr hModule, [MarshalAs(UnmanagedType.LPStr)] string procName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);

        //The function determins whether a method exists in the export table of a certain module. 
        private static bool Win32MethodExist(string moduleName, string methodName)
        {
            IntPtr moduleHandle = GetModuleHandle(moduleName);

            if (moduleHandle == IntPtr.Zero)
                return false;

            return GetProcAddress(moduleHandle, methodName) != IntPtr.Zero;
        }

        public static bool Is64BitOperatingSystem()
        {
            // 64-bit programs run only on Win64
            if (IntPtr.Size == 8)
                return true;

            // 32-bit programs run on both 32-bit and 64-bit Windows 
            bool flag;
            return Win32MethodExist("kernel32.dll", "IsWow64Process") &&
                   IsWow64Process(GetCurrentProcess(), out flag) && flag;
        }

        public static string GetOsVersion(bool checkSP)
        {
            OperatingSystem os = Environment.OSVersion;

            string operatingSystem = "Unknown";

            if (os.Platform == PlatformID.Win32Windows)
            {
                //This is a pre-NT version of Windows 
                switch (os.Version.Minor)
                {
                    case 0:
                        operatingSystem = "95";
                        break;
                    case 10:
                        if (os.Version.Revision.ToString() == "2222A")
                            operatingSystem = "98SE";
                        else
                            operatingSystem = "98";
                        break;
                    case 90:
                        operatingSystem = "Me";
                        break;
                    default:
                        break;
                }
            }
            else if (os.Platform == PlatformID.Win32NT)
            {
                switch (os.Version.Major)
                {
                    case 3:
                        operatingSystem = "NT 3.51";
                        break;
                    case 4:
                        operatingSystem = "NT 4.0";
                        break;
                    case 5:
                        if (os.Version.Minor == 0)
                            operatingSystem = "2000";
                        else
                            operatingSystem = "XP";
                        break;
                    case 6:
                        if (os.Version.Minor == 0)
                            operatingSystem = "Vista";
                        else
                            operatingSystem = "7";
                        break;
                    default:
                        break;
                }
            }

            //Make sure we actually got something in our OS check 
            //We don't want to just return " Service Pack 2" or " 32-bit" 
            if (operatingSystem != "")
            {
                operatingSystem = "Windows " + operatingSystem;

                if (os.ServicePack != "" && checkSP)
                    operatingSystem += " " + os.ServicePack;
            }

            return operatingSystem;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Use: 
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class AdnTimer
    {
        private DateTime _previous;

        public AdnTimer()
        {
            _previous = DateTime.Now;
        }

        public double ElapsedSeconds
        {
            get
            {
                DateTime Now = DateTime.Now;
                TimeSpan Elaspsed = Now.Subtract(_previous);

                _previous = Now;

                return Elaspsed.TotalSeconds;
            }
        }
    }
}