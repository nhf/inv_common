﻿namespace Inv.Common._Core
{
    public interface INamedWrapper
    {
        string Name { get; }
    }
}