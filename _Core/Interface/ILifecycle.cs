﻿namespace Inv.Common._Core.Interface
{
    public interface ILifecycle
    {
        void Start();
        void Enable();
        void Disable();
        void Stop();
    }
}