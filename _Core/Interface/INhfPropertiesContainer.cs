﻿using Inv.Common.R;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common._Core.Interface
{
    public interface INhfPropertiesContainer
    {
        string PartNum { get; set; }

        string SelectedPartClass { get; set; }

        string ProjectNum { get; set; }

        string BarNum { get; set; }

        string Description { get; set; }

        string Alloy { get; set; }

        string Finish { get; set; }

        bool Is2D { get; set; }

        string Length { get; set; }

        string Height { get; set; }

        string MfgMethod { get; set; }

        string AnalysisCode { get; set; }

        string SelectedBomStructure { get; set; }
    }
}
