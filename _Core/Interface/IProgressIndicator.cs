﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common.Adapter.InventorAdapters
{
    public interface IProgressIndicator
    {
        void Create(int total, string text, int progress =0, bool autoFinish=true);

        void Increment();

        void UpdateProgress(int progress);

        void AddToTotal(int additional);

        void Finish();
    }
}
