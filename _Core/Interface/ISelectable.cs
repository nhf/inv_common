﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common._Core.Interface
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }
}
