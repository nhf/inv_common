﻿using Inv.Common._Core;
using Inv.Common._Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common.R
{

    public static class AnalysisCodes
    {

        public static AnalysisCode[] AllowedAnalysisCodesFor(ClassId classId)
        {
            return ((AnalysisCode[])(object)costCodesTriage(classId));
        }

        public static AnalysisCode[] AllowedAnalysisCodesFor(string classIdStr)
        {
            ClassId partClass;
            if (Enum.TryParse(classIdStr, out partClass))
            {
                return ((AnalysisCode[])(object)costCodesTriage(partClass));
            }
            return new AnalysisCode[] { };
        }

        private static int[] costCodesTriage(ClassId classId)
        {
            switch (classId)
            {
                case ClassId.AA:
                    return new int[] { 30203, 30305 };
                case ClassId.AF:
                    return new int[] { 30602 };
                case ClassId.AM:
                    return new int[] { 30102, 30103, 30202, 30203, 30204, 30230 };
                case ClassId.AS:
                    return new int[] { 30102, 30202, 30203, 30204, 30230 };
                case ClassId.BA:
                    return new int[] { 30306, 30801, 30803, 30805, 30810, 30812, 30820, 30830 };
                case ClassId.BM:
                    return new int[] { 30306, 30804, 30805, 30812, 30813, 30820, 30830 };
                case ClassId.BS:
                    return new int[] { 30802, 30805, 30811, 30812, 30820, 30830 };
                case ClassId.CA:
                    return new int[] { 30304 };
                case ClassId.EA:
                    return new int[] { 30101 };
                case ClassId.EG:
                    return new int[] { 30402 };
                case ClassId.EP:
                    return new int[] { 30303, 30451, 30603 };
                case ClassId.EX:
                    return new int[] { 30201, 30203, 30207, 30208, 30302, 30306, 30308 };
                case ClassId.FG:
                    return new int[] { 30402 };
                case ClassId.FS:
                    return new int[] { 30206, 31301, 31302, 31310 };
                case ClassId.GL:
                    return new int[] { 30701, 30702, 30703, 30704, 30705, 30706, 30707 };
                case ClassId.GZ:
                    return new int[] { 30501, 30502 };
                case ClassId.HW:
                    return new int[] { 30950, 31120, 31131, 31132, 31250, 31602 };
                case ClassId.IN:
                    return new int[] { 30901, 30902, 30903, };
                case ClassId.SA:
                    return new int[] { 31130, 30205, 30840 };
                case ClassId.ST:
                    return new int[] { 31211, 31230 };
                case ClassId.TF:
                    return new int[] { 30601, 30602 };
                case ClassId.WD:
                    return new int[] { 31401 };
                default:
                    return new int[0];
            }
        }
    }

    public enum AnalysisCode
    {
        [Description("None")]
        None = 0,

        [Description("Kickers")]
        Kickers = 30203,

        [Description("Foam Blocks")]
        FoamBlocks = 30602,

        [Description("Channels")]
        Channels = 30102,

        [Description("Shear Splices/Lugs")]
        ShearSplicesLugs = 30306,

        [Description("Stainlesss Steel Sheet")]
        StainlessSheet = 30802,

        [Description("Castings")]
        Castings = 30304,

        [Description("Built up embed boxes")]
        EmbedBox = 30101,

        [Description("Gaskets")]
        Gaskets = 30402,

        [Description("Thermal Struts")]
        ThermalStruts = 30303,

        [Description("Aluminum Anchor Base Plates")]
        AluAnchorBasePlate = 30201,

        [Description("Anchor Fasteners")]
        AnchorFastener = 30206,

        [Description("Shop Sealant")]
        ShopSealant = 30501,

        [Description("Insulation Accessories")]
        InsulationAccessories = 30950,

        [Description("CW Insulation Sheets")]
        CwInsulationSheets = 30901,

        [Description("Operable Vent")]
        OperableVent = 31130,

        [Description("Types")]
        Types = 31211,

        [Description("Tape")]
        Tape = 30601,

        [Description("Crating Wood")]
        CratingWood = 31401,

        [Description("Standard Aluminum Extrusions")]
        StdAluExtrusion = 30305,

        [Description("Steel/Misc. Steel Embeds")]
        Steel = 30103,

        [Description("Steel Anchors")]
        SteelAnchors = 30202,

        [Description("Exterior Features")]
        ExtFeatures = 30805,

        [Description("Plastics")]
        Plastics = 30451,

        [Description("Standard Fasteners")]
        StdFasteners = 31301,

        [Description("Field Sealant")]
        FieldSealant = 30502,

        [Description("Hardware")]
        Hardware = 31120,

        [Description("CW Pre-cut Insulation Sheets")]
        CwPrecutInsulationSheets = 30902,

        [Description("Aluminum Anchor Assembly")]
        AluAnchorAssembly = 30205,

        [Description("Terracotta")]
        Terracotta = 31230,

        [Description("Aluminum Sheet")]
        AluSheet = 30801,

        [Description("Galvanized Steel Sheet")]
        GalvSteelSheet = 30804,

        [Description("Prefabricated SS Panels")]
        PrefabSSPanels = 30811,

        [Description("Shims/Spacers")]
        ShimsSpacers = 30603,

        [Description("Anchor Hook Fabrication (Buyout)")]
        AnchorHookFabBuyout = 30207,

        [Description("Specialty Fasteners")]
        SpecialtyFasteners = 31302,

        [Description("Operable Vent Hardware")]
        OperableVentHardware = 31131,

        [Description("Rigid Insulation")]
        RigidInsulation = 30903,

        [Description("Shadow Box Assembly")]
        ShadowboxAssembly = 30840,

        [Description("Misc. Steel")]
        MiscSteel = 30204,

        [Description("Composite Sheet")]
        CompositeSheet = 30803,

        [Description("Prefabricated Comp. Panels")]
        PrefabCompPanels = 30812,

        [Description("Anchor Slide Fabrication (Buyout)")]
        AnchorSlideFabBuyout = 30208,

        [Description("Field Fasteners")]
        FieldFasteners = 31310,

        [Description("Trickle Vent")]
        TrickleVent = 31132,

        [Description("Reinforcing Steel (Mullions)")]
        ReinforcingSteelMullions = 30230,

        [Description("Flashings")]
        Flashings = 30820,

        [Description("Aluminum Extrusions")]
        AluExtrusions = 30302,

        [Description("Stone Accessories")]
        StoneAccessories = 31250,

        [Description("Prefabricated Alum. Panels")]
        PrefabAluPanels = 30810,

        [Description("Prefabricated Galv. Panels")]
        PrefabGalvPanels = 30813,

        [Description("Coping/Coping Panels")]
        CopingPanels = 30830,

        [Description("Window Washing Button")]
        WindowWashingButton = 31602,

        [Description("Specialty Extrusions")]
        SpecialtyExtrusions = 30308,

        [Description("GL-01")]
        GL01 = 30701,

        [Description("GL-02")]
        GL02 = 30702,

        [Description("GL-03")]
        GL03 = 30703,

        [Description("GL-04")]
        GL04 = 30704,

        [Description("GL-05")]
        GL05 = 30705,

        [Description("GL-06")]
        GL06 = 30706,

        [Description("GL-07")]
        GL07 = 30707
    }
}
