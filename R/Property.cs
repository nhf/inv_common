﻿namespace Inv.Common.R
{
#if VAULT2
    public class Property
    {
        // inventor default Const key strings
        public const string KeyPartNum = "Part Number";
        public const string KeyProject = "Project";
        public const string KeyDescription = "Description";
        public const string KeyCompany = "Company";

        // Custom iProperty Const key strings
        public const string KeyClassId = "CLASS_ID";
        public const string KeyLength = "LENGTH";
        public const string KeyWidth = "WIDTH";
        public const string KeyHeight = "HEIGHT";
        public const string KeyDepth = "DEPTH";
        public const string KeyAlloy = "ALLOY";
        public const string KeyFinish = "FINISH";
        public const string KeyBarNumber = "BAR_NUMBER";
        public const string KeyTypeCode = "TYPE_CODE";
        public const string KeyEpicorDesc = "EPICOR DESCRIPTION";

        public const string KeyApproved = "APPROVED";
        public const string KeyAnalysisCode = "ANALYSIS_CODE";
        public const string KeyCostCode = "COST_CODE";
        public const string KeyCostMethod = "COST_METHOD";
        public const string KeyEcoGroupId = "ECO_GROUP_ID";
        public const string KeyLaborEntryMethod = "LABOR_ENTRY_METHOD";
        public const string KeyMachines = "MACHINES";
        public const string KeyPlant = "PLANT";
        public const string KeyQtyBearing = "QTY_BEARING";
        public const string KeyRelatedOperation = "RELATED_OPERATION";
        public const string KeyStdFormat = "STD_FORMAT";
        public const string KeyUpdatePartPlant = "UPDATE_PART_PLANT";
        public const string KeyTrackSerialNum = "TRACK_SERIAL_NUM";
        public const string KeyMtlPartNum = "MTL_PART_NUM";

        public const string KeyLotMfgBatch = "LOT_MFG_BATCH";
        public const string KeyTrackLots = "TRACK_LOTS";
        public const string KeyUomClassId = "UOM_CLASS_ID";
        public const string KeyPlanAsAsm = "PLAN_AS_ASM";
        public const string KeyViewAsAsm = "VIEW_AS_ASM";


        // Const values strings
        public const string ValEpicorDesc = @"=<Description>, ";
        public const string ValEpicorDescMetal = @"=<Description>, <ALLOY>, <FINISH>, ";
        public const string ValEpicorDescDims1D = @"Length= <LENGTH>";
        public const string ValEpicorDescDims2D = @"Width= <WIDTH>, Height= <HEIGHT>";
    }
#endif

#if VAULT3
    public class Property
    {
        // inventor default Const key strings
        public const string KeyPartNum = "Part Number";
        public const string KeyProject = "Project";
        public const string KeyDescription = "Description";
        public const string KeyCompany = "Company";

        // Custom iProperty Const key strings
        public const string KeyClassId = "ClassID";
        public const string KeyLength = "Length";
        public const string KeyWidth = "Width";
        public const string KeyHeight = "Height";
        public const string KeyDepth = "Depth";
        public const string KeyAlloy = "Alloy";
        public const string KeyFinish = "Finish";
        public const string KeyBarNumber = "MtlPartNum";
        public const string KeyTypeCode = "TypeCode";
        public const string KeyEpicorDesc = "Epicor Description";

        public const string KeyApproved = "Approved";

        // I disagree with this change - this should be "AnalysisCode"
        // TODO: change this back (remove CostCode)
        public const string KeyAnalysisCode = "AnalysisCode";
        public const string KeyCostCode = "CostCode";

        public const string KeyCostMethod = "CostMethod";
        public const string KeyEcoGroupId = "ECOGroupID";
        public const string KeyLaborEntryMethod = "LABOR_ENTRY_METHOD";
        public const string KeyMachines = "MACHINES";
        public const string KeyPlant = "Plant";
        public const string KeyQtyBearing = "QtyBearing";
        public const string KeyRelatedOperation = "RelatedOperation";
        public const string KeyStdFormat = "STD_FORMAT";
        public const string KeyUpdatePartPlant = "UpdatePartPlant";
        public const string KeyTrackSerialNum = "Track Serial Num";
        public const string KeyMtlPartNum = "MtlPartNum";

        public const string KeyLotExpDate = "LotExpDt";
        public const string KeyLotMfgBatch = "LotMfgBatch";
        public const string KeyTrackLots = "TrackLots";
        public const string KeyUomClassId = "UOMClassID";
        public const string KeyPlanAsAsm = "PLAN_AS_ASM";
        public const string KeyViewAsAsm = "ViewAsAsm";
    }
#endif
}