﻿using Inventor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common.R
{
    internal class BomStructureAttr : System.Attribute
    {
        internal string Name;
        internal BOMStructureEnum InvBomStructure;

        internal BomStructureAttr(string name, BOMStructureEnum mapped)
        {
            Name = name;
            InvBomStructure = mapped;
        }
    }


    public static class BomStructures
    {
        public static string Name(this BomStructure bs)
        {
            return GetAttr(bs).Name;
        }

        public static BOMStructureEnum InventorBOMStructure(this BomStructure bs)
        {
            return GetAttr(bs).InvBomStructure;
        }

        private static BomStructureAttr GetAttr(BomStructure bs)
        {
            return (BomStructureAttr)System.Attribute.GetCustomAttribute(ForValue(bs), typeof(BomStructureAttr));
        }

        private static MemberInfo ForValue(BomStructure bs)
        {
            return typeof(BomStructure).GetField(Enum.GetName(typeof(BomStructure), bs));
        }

        public static BomStructure FromInventor(BOMStructureEnum invBom)
        {
            switch (invBom)
            {
                case BOMStructureEnum.kInseparableBOMStructure:
                    return BomStructure.Inseperable;
                case BOMStructureEnum.kNormalBOMStructure:
                    return BomStructure.Normal;
                case BOMStructureEnum.kPhantomBOMStructure:
                    return BomStructure.Phantom;
                case BOMStructureEnum.kPurchasedBOMStructure:
                    return BomStructure.Purchased;
                case BOMStructureEnum.kReferenceBOMStructure:
                    return BomStructure.Reference;
                case BOMStructureEnum.kDefaultBOMStructure:
                    return BomStructure.Default;
                default:
                    return BomStructure.Default;
            }
        }
    }

    public enum BomStructure
    {
        [BomStructureAttr("Default", BOMStructureEnum.kDefaultBOMStructure)]
        Default,
        [BomStructureAttr("Normal", BOMStructureEnum.kNormalBOMStructure)]
        Normal,
        [BomStructureAttr("Phantom", BOMStructureEnum.kPhantomBOMStructure)]
        Phantom,
        [BomStructureAttr("Reference", BOMStructureEnum.kReferenceBOMStructure)]
        Reference,
        [BomStructureAttr("Purchased", BOMStructureEnum.kPurchasedBOMStructure)]
        Purchased,
        [BomStructureAttr("Inseperable", BOMStructureEnum.kInseparableBOMStructure)]
        Inseperable
        /*
        [BomStructureAttr("Varies", BOMStructureEnum.kVariesBOMStructure)]
        Varies
        */
    }
}
