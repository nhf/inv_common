using Inv.Common._Core.Base;
using Inv.Common._Core.Extensions;
using System;
using System.Reflection;

namespace Inv.Common._Core
{
    public enum ClassId
    {
        [Description("None")]
        None = 0,
        [Description("AA")]
        AA,
        [Description("AF")]
        AF,
        [Description("AM")]
        AM,
        [Description("AS")]
        AS,
        [Description("BA")]
        BA,
        [Description("BM")]
        BM,
        [Description("BS")]
        BS,
        [Description("CA")]
        CA,
        [Description("DE")]
        DE,
        [Description("EA")]
        EA,
        [Description("EG")]
        EG,
        [Description("EP")]
        EP,
        [Description("EX")]
        EX,
        [Description("FA")]
        FA,
        [Description("FG")]
        FG,
        [Description("FS")]
        FS,
        [Description("GL")]
        GL,
        [Description("GZ")]
        GZ,
        [Description("HW")]
        HW,
        [Description("IN")]
        IN,
        [Description("KT")]
        KT,
        [Description("RF")]
        RF,
        [Description("SA")]
        SA,
        [Description("ST")]
        ST,
        [Description("TF")]
        TF,
        [Description("UA")]
        UA,
        [Description("WD")]
        WD,
        [Description("WF")]
        WF
    }
}