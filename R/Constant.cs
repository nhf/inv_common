﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common.R
{
    public static class Constant
    {
        public const string nvmKey_SaveContext_TopLevelSaveFileName = "TopLevelSaveFileName";
        public const string nvmKey_SaveContext_SaveCopyAsFileName = "SaveCopyAsFilename";
        public const string nvmKey_SaveContext_SaveFileName = "SaveFileName";
    }
}
