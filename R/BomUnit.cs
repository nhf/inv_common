﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common.R
{
    internal class BomUnitsAttr : System.Attribute
    {
        internal string Name;
        internal Inventor.UnitsTypeEnum BomUnitType;
        internal Inventor.BOMQuantityTypeEnum BomQtyType;

        internal BomUnitsAttr(string name, Inventor.UnitsTypeEnum mapped)
        {
            Name = name;
            BomUnitType = mapped;
        }
    }

    public enum BomUnit
    {
    }
}
