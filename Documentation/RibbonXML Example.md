﻿<?xml version="1.0" encoding="utf-8"?>
<!--XML Ribbon/Tab, Panel, and Button descriptinos-->

<!-- Example usage:
    //Check for ribbon.override.xml -> provides the user a way to override the 
    //ribbon settings embedded in the add-in once installed.
    //The user can just place an xml file named "asmName.ribbons.override.xml"
    //at the same location than the add-in dll and replacing "asmName" with the 
    //actual name of the assembly dll

    //Check for assembly.ribbon.override.xml first
    string asmName = System.Reflection.Assembly.GetExecutingAssembly().Location;
    asmName = asmName.Remove(asmName.Length - 4, 4);

    if (System.IO.File.Exists(asmName + ".ribbons.override.xml"))
    {
        AdnRibbonBuilder.CreateRibbonFromFile(
            m_inventorApplication,
            addinType,
            asmName + ".ribbons.override.xml");
    }
    else
    {
        AdnRibbonBuilder.CreateRibbon(
            m_inventorApplication,
            addinType,
            "DogboneFeatureAddin.resources.ribbons.xml");
    }
-->

<Ribbon>

  <RibbonTab ribbons="Part"
           internalName="id_TabModel" 
           displayName="" 
           targetTab="" 
           insertBefore="false">
  
    <RibbonPanel internalName="id_PanelP_ModelModify" 
                 displayName=""
                 targetPanel="" 
                 insertBefore="false">

      <Button internalName="Autodesk.DogboneFeatureAddin.DogboneFeatureCmd" 
                useLargeIcon="false" 
                isInSlideout="true" 
                targetControl="" 
                insertBefore="false"/>
      
    </RibbonPanel>

  </RibbonTab>

  <RibbonTab ribbons="Assembly"
          internalName="id_TabModel"
          displayName=""
          targetTab=""
          insertBefore="false">

    <RibbonPanel internalName="id_PanelA_ModelModify"
                  displayName=""
                  targetPanel=""
                  insertBefore="false">

      <Button internalName="Autodesk.DogboneFeatureAddin.DogboneFeatureCmd"
                useLargeIcon="false"
                isInSlideout="true"
                targetControl=""
                insertBefore="false"/>

    </RibbonPanel>
  
</RibbonTab>

</Ribbon>
