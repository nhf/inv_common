﻿using Nhf.Inv.Common.ThreadedDelegate.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inv.Common.ThreadedDelegate.Base
{
    public sealed class Delegator<T>
    {
        private IDelegateCallback<T> callback;

        private Queue<T> workQueue = new Queue<T>();

        private volatile bool _cancelled;

        private volatile bool _isRunning;

        private volatile int _threadCount;


        public Delegator(IDelegateCallback<T> listener)
        {
            callback = listener;
        }

        public void Enqueue(T t)
        {
            _cancelled = false;

            lock (workQueue)
            {
                workQueue.Enqueue(t);
                
                if (!_isRunning)
                {
                    spinUp();
                }
            }
        }

        private T dequeue()
        {
            lock (workQueue)
            {
                return workQueue.Dequeue();
            }
        }

        private bool isQueueEmpty()
        {
            lock (workQueue)
            {
                return workQueue.Count == 0;
            }
        }

        private void spinUp()
        {
            _isRunning = true;

            Thread t = new Thread(() =>
            {
                while (!isQueueEmpty())
                {
                    Interlocked.Increment(ref _threadCount);
                    ThreadPool.QueueUserWorkItem(
                        (object context) =>
                        {
                            try
                            {
                                callback.OnExecute((T)context);
                            }
                            catch
                            {
                                // Squash
                            }
                            finally
                            {
                                if (Interlocked.Decrement(ref _threadCount) < 1)
                                {
                                    if (isQueueEmpty())
                                    {
                                        _isRunning = false;

                                        if (!_cancelled)
                                        {
                                            callback.OnFinished();
                                        }
                                    }
                                    else
                                    {
                                        if (!_cancelled)
                                        {
                                            spinUp();
                                        }
                                    }
                                }
                            }
                        }, dequeue());
                }
            });

            t.SetApartmentState(ApartmentState.MTA);
            t.Start();
        }

        public void Cancel()
        {
            _cancelled = true;
            lock (workQueue)
            {
                workQueue.Clear();
            }
            _threadCount = 0;
        }
    }
}
