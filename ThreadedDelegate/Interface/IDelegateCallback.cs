﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nhf.Inv.Common.ThreadedDelegate.Interface
{
    public interface IDelegateCallback<T>
    {
        bool OnExecute(T t);

        bool OnFinished(); 
    }
}
