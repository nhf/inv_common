# README 

inv_common contains all of the convenience classes, wrappers, and adapters that I wrote to insulate my code from the stupidity present in the Inventor API. Every time I started using something new in the API and started asking myself why they would do it how they did it, I wrote a wrapper/adapter using modern programming patterns to try and prevent some headache.  

* Adapter - general Adapter classes
    * Inv_Adapter - Inventor specific Adapter classes (Properties, Paramaters, BOM, Attributes, etc)
* Documentation - contains Documentation that I created to fill in gaps in what Autodesk provides. Has some info that I had to dig out with a lot of trial and error and didn't want to get lost. The ribbon/tab/panel map is particularly useful as a reference for finding existing panels and buttons in the Ribbon.
* R - my common Constants and Enums
* Threaded Delegate - an abstract, flexible, Queue-based multithreaded worker. This is an _extremely_ powerful module for multithreading arbitrary work. It's the worker that App.Nasdaq uses to multithread its Part parsing, but I have written it to be capable of executing any parallelizable work. It's also pretty complex (statements are nested 10 deep in the work methods) but it works!
* _Core - everything else
    * Adn - some code I borrowed (and modified a bit) from one of Autodesk's community managers. There are some convenience classes in here for instantiating an add-in and inserting buttons, binding commands, working with Part geometry, etc. All of my AddInServer classes derive from and use code located here.
    * Base - all my Base (un-instantiable) classes that get derived elsewhere to make useful things.
    * Exceptions - custom exceptions
    * Extensions - custom utilities using the awesome C# feature of extension methods
    * Interface <-
    * Util <-