﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

#endregion

namespace Inv.Common.Adapter
{
    /// <summary>
    ///     Wrapper class around some Inventer File operations along with low level IO
    /// </summary>
    public static class FileUtil
    {
        public static string GetAppdataDir()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }

        public static bool TryValidatePath(string expectedExtension, string fullFileName, out string validatedFileName)
        {
            if (string.IsNullOrEmpty(fullFileName))
            {
                validatedFileName = "";
                return false;
            }

            try
            {
                if (!Path.HasExtension(fullFileName))
                {
                    fullFileName += expectedExtension;
                }
                else if (Path.GetExtension(fullFileName) != expectedExtension)
                {
                    string fileDir = Path.GetDirectoryName(fullFileName);
                    string fileName = Path.GetFileNameWithoutExtension(fullFileName);
                    fileName += expectedExtension;
                    if (fileDir != null)
                    {
                        fullFileName = Path.Combine(fileDir, fileName);
                    }
                }
                validatedFileName = fullFileName;
                return true;
            }
            catch
            {
                validatedFileName = "";
                return false;
            }
        }

        /// <summary>
        ///     Takes the full path of a file and a new filename and returns the new file full path.
        /// </summary>
        /// <param name="oldFullPath">The existing full path including old filename and extension.</param>
        /// <param name="newFileName">The new, bare filename without extension.</param>
        /// <returns>The full path of the new file, with extension.</returns>
        public static string NewFullPath(string oldFullPath, string newFileName)
        {
            return Path.Combine(Path.GetDirectoryName(oldFullPath), newFileName) + Path.GetExtension(oldFullPath);
        }

        public static HashSet<string> GetDirectoryContentsRecursor(HashSet<string> contents, string directory)
        {
            try
            {
                // Base case, there are some files in this dir
                foreach (string search in new[] { "*.ipt", "*.iam" })
                {
                    foreach (string f in Directory.GetFiles(directory, search))
                    {
                        contents.Add(f);
                    }
                }

                // Recursor - there are subdirs in this dir
                foreach (
                    string d in
                        Directory.GetDirectories(directory)
                            .Where(dir => !dir.EndsWith("_V") && !dir.EndsWith("OldVersions")))
                {
                    // Recurse
                    GetDirectoryContentsRecursor(contents, d);
                }
            }
            catch
            {
                // Ignore
            }
            return contents;
        }
    }
}