﻿#region

using System.Collections.Generic;

#endregion

namespace Inv.Common.Adapter
{
    /// <summary>
    ///     This interface aims to provide a single comprehensive way to access Data from any of the several
    ///     base Inventor data storage locations. See implementors for specifics an how each of those provide
    ///     access to the underlying Data.
    /// </summary>
    public interface IDataSet
    {
        string Name { get; set; }

        /// <summary>
        ///     Returns the value of the object in the underlying datastore. If you want the object, use <see cref="Get(string)" />
        /// </summary>
        /// <param name="key"></param>
        /// <returns>The value of the object in the underlying datastore.</returns>
        object this[string key] { get; set; }

        /// <summary>
        ///     Returns a dictionary of key, value pairs where the key is the equivalent indexer and the value is the data struct
        ///     object.
        /// </summary>
        /// <remarks> This returns the objects themselves, not the values of the underlying objects</remarks>
        IDictionary<string, object> Items { get; }

        /// <summary>
        ///     Returns the object from the underlying datastore. To get the value of the object, use <see cref="this[string]" />
        /// </summary>
        /// <param name="key">The data struct object from the underlying datastore.</param>
        /// <returns></returns>
        object Get(string key);

        /// <summary>
        ///     Checks for the existence of an object specified by the passed key string in the datastore.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Contains(string key);

        /// <summary>
        ///     Adds a new object to the datastore with the passed key and value. Makes a reaonable attempt to turn the (k,v) in to
        ///     a
        ///     fully qualified data struct object, but results may vary.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        void Add(string key, object val);

        /// <summary>
        ///     Deletes the data struct object specified by the passed key from the underlying datastore, if it exists.
        /// </summary>
        /// <param name="key"></param>
        void Delete(string key);

        bool IsEmpty();
    }
}