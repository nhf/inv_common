﻿namespace Inv.Common.Adapter
{
    /// <summary>
    ///     Wrapper class for Part Documents. Contains several methods to make it easier to access
    ///     Inventor properties/routines of those Documents.
    /// </summary>
    public class NhfPart : NhfComponent
    {
        internal NhfPart()
        {
        }
    }
}