﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Inv.Common.Properties;
using Inv.Common._Core;
using Inventor;
using Path = System.IO.Path;
using Inv.Common.Adapter.InventorAdapters;
using Inv.Common._Core.Extensions;
using Inv.Common._Core.Util;

#endregion

namespace Inv.Common.Adapter
{
    /// <summary>
    ///     This class defines the contract of part numbering that we expect everything we work with to adhere to.
    ///     Parts are expected to conform to this scheme:
    ///     1003EX001-00001
    ///     EX001-00001
    ///     which breaks down as:
    ///     1003 - Project code
    ///     EX - Part Class
    ///     001 - Profile code
    ///     00001 - Engineering/machining code
    /// </summary>
    public abstract class NhfComponent
    {
        private const int ProjectLen = 4;
        private const int ClassLen = 2;
        private const int ProfileLen = 3;
        private const int EngCodeLen = 5;
        private readonly char _partDelim = Resources.DELIMITER_DASH[0];

        public ClassId ClassCode;
        public string Special;
        public string FullPath;
        public int ProfileCode;
        public string ProjectNum;
        public int SequentialNum;

        public static string NextSequentialFullPath(Document doc)
        {
            string docFullPath = doc.FullFileName;
            return NextSequentialFullPath(docFullPath);
        }

        public static string NextSequentialFullPath(string docFullPath)
        {
            NhfComponent component = FromPath(docFullPath);
            if (component == null) return "";

            string workingDir = Path.GetDirectoryName(docFullPath);
            if (workingDir == null) return "";

            string[] files = Directory.GetFiles(workingDir);
            IEnumerable<string> existingComponents = files.Select(FromPath)
                .Where(c => c != null)
                .Where(c => c.ClassCode == component.ClassCode)
                .Where(c => c.ProfileCode == component.ProfileCode)
                .Select(c => c.FullPath);

            int[] usedSeqNums = existingComponents.Select(Path.GetFileNameWithoutExtension)
                .Select(FromPath)
                .Select(c => c.SequentialNum)
                .Distinct().ToArray();

            int nextSeqNum = usedSeqNums.LowestUnused();

            component.SequentialNum = nextSeqNum;
            string newFileName = component.ToString();

            return FileUtil.NewFullPath(docFullPath, newFileName);
        }

        public static NhfComponent From(Document doc)
        {
            string fullFileName = doc.FullFileName;
            if (!string.IsNullOrEmpty(fullFileName))
            {
                return FromPath(fullFileName);
            }

            try
            {
                string partNumber = (string)new PropertySetAdapter(doc, PropertySetAdapter.PropertysetsDesign)?[R.Property.KeyPartNum];
                if (partNumber != null)
                {
                    return FromPartNumber(partNumber);
                }
            }
            catch
            {
                // Squash
            }
            return null;
        }

        /// <summary>
        ///     Parses the passed file path and returns fully composed NhfPart or NhfAssembly objects.
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static NhfComponent FromPath(string fullPath)
        {
            string partNumber = Path.GetFileNameWithoutExtension(fullPath);
            NhfComponent c = FromPartNumber(partNumber);
            if (c != null)
            {
                c.FullPath = fullPath;
            }
            return c;
        }

        [Obsolete]
        private static NhfComponent FromPartNumber2(string partNumber)
        {
            NhfComponent component;

            // Make sure there is a part number
            if (string.IsNullOrEmpty(partNumber))
            {
                // If not, return a base case
                return component = new NhfPart();
            }

            string[] tokens = TokenizePartNumber(partNumber);

            ClassId classCode = GetClassId(tokens[0]);

            switch (classCode)
            {
                case ClassId.UA:
                case ClassId.SA:
                case ClassId.FA:
                    component = new NhfAssembly();
                    break;
                default:
                    component = new NhfPart();
                    break;
            }

            component.ClassCode = classCode;
            component.ProjectNum = GetProjectNumber(tokens[0]);

            string remnant = GetProjectClassRemnant(tokens[0], component.ClassCode);


            string profile = "";
            string seq = "";
            string special = "";

            switch (classCode)
            {
                case ClassId.FS:
                    profile = remnant;
                    special = tokens.Length > 1 ? tokens[1] : "";
                    break;
                case ClassId.GL:
                case ClassId.ST:
                    if (tokens.Length > 2)
                    {
                        profile = tokens[1];
                        seq = tokens[2];
                        special = remnant;
                    }
                    break;
                case ClassId.GZ:
                    // GZ1M000 - 00 # TYP SEALANT
                    //0000GZ000 - 00000 # TYPE ENGINEERED SILICONE GASKETS
                    if (string.IsNullOrEmpty(component.ProjectNum))
                    {
                        special = remnant;
                        seq = tokens.Length > 1 ? tokens[1] : "";
                        break;
                    }
                    goto default;
                case ClassId.SA:
                case ClassId.FA:
                    // SA/FA differ from UA by categorization
                    if (tokens.Length > 1)
                    {
                        // The VAULT3 SA scheme is SAXXX-##### where 
                        // XXX is a 3 digit SA identifier indicating the type or family, and 
                        // ##%## is a 5 digit sequential code
                        profile = remnant;
                        seq = tokens[1];
                    }
                    else
                    {
                        // VAULT2 uses a standard 5 digit SA numbering scheme, similar to UAs
                        if (remnant.Length >= 2)
                        {
                            profile = remnant.Substring(0, 2);
                        }

                        if (remnant.Length >= 5)
                        {
                            seq = remnant.Substring(2, 3);
                        }
                    }
                    break;
                case ClassId.UA:
                    seq = remnant;
                    special = tokens.Length > 1 ? tokens[1] : "";
                    break;
                default:
                    // Everything that conforms to the 0000AA000-00000
                    profile = remnant;
                    seq = tokens.Length > 1 ? tokens[1] : "";
                    break;
            }

            try
            {
                component.ProfileCode = Convert.ToInt32(profile);
            } catch { };
            try
            {
                component.SequentialNum = Convert.ToInt32(seq);
            } catch { };

            component.Special = special;
            return component;
        }

        public static ClassId GetClassId(string partNumber)
        {
            string classIdStr = partNumber.Contains(EnumAttrUtil.GetNames<ClassId>());
            return EnumAttrUtil.GetEnumFromDescription(classIdStr, ClassId.None);
        }

        public static string GetProjectNumber(string partNumber, ClassId classCode=ClassId.None)
        {
            if (classCode == ClassId.None)
            {
                classCode = GetClassId(partNumber);
            }

            if (classCode == ClassId.None)
            {
                return "";
            }

            int indexOfClass = partNumber.IndexOf(EnumAttrUtil.Description(classCode));
            if (indexOfClass < 1)
            {
                return "";
            }

            return partNumber.Substring(0, indexOfClass);
        }

        private static string GetProjectClassRemnant(string partNumber, ClassId classCode=ClassId.None)
        {
            if (classCode == ClassId.None)
            {
                classCode = GetClassId(partNumber);
            }

            if (classCode == ClassId.None)
            {
                return "";
            }

            int indexOfClass = partNumber.IndexOf(EnumAttrUtil.Description(classCode));
            if (indexOfClass < 1)
            {
                return "";
            }

            if (partNumber.Length < indexOfClass + EnumAttrUtil.Description(classCode).Length)
            {
                return "";
            }

            int indexOfRemnant = indexOfClass + EnumAttrUtil.Description(classCode).Length;
            return partNumber.Substring(indexOfRemnant, partNumber.Length - indexOfRemnant);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="partNumber"></param>
        /// <returns></returns>
        [Obsolete]
        private static NhfComponent FromPartNumber(string partNumber)
        {
            NhfComponent component;

            // Make sure there is a part number
            if (string.IsNullOrEmpty(partNumber))
            {
                // If not, return a base case
                return component = new NhfPart();
            }

            string[] tokens = TokenizePartNumber(partNumber);


            // Get the Project Number and the Class Code
            // Fully numerical project numbers cause an issue here because integers can be cast to 
            // the enum type. So the first 2 chars of a numerical project number are valid Class Code enums.
            // We handle that edge case by trying to convert the entire proj number to an int, and only trying
            // the enum parse if that fails.

            ClassId classCode = ClassId.None;
            string project = "";

            if (tokens[0].Length > ProjectLen + ClassLen)
            {
                // Determine project and class
                try
                {
                    // Test if there is a leading Project Number (4 digits)
                    tokens[0].TrySubstring(0, ProjectLen, out project);
                    Convert.ToInt32(project);

                    // First 4 are project number, try and get class code from 5 and 6
                    string classStr = "";
                    tokens[0].TrySubstring(4, ClassLen, out classStr);
                    Enum.TryParse(classStr, out classCode);
                }
                catch
                {
                    // Not a 4 digit project number. Test if digits 1 and 2 are valid Class Code chars
                    string classStr = "";
                    if (tokens[0].TrySubstring(0, ClassLen, out classStr))
                    {
                        if (EnumAttrUtil.IsValid<ClassId>(classStr))
                        {
                            // 1 and 2 are class code
                            Enum.TryParse(classStr, out classCode);
                            // unset the project
                            project = "";
                        }
                    }
                    else
                    {
                        // 1 and 2 are not class code, might be unusual project number
                        // Don't do anything to project, test 5 and 6 for class code
                        
                        if (tokens[0].TrySubstring(ProjectLen, ClassLen, out classStr))
                        {
                            if (EnumAttrUtil.IsValid<ClassId>(classStr))
                            {
                                // 5 and 6 are class, set the enum and let's go home
                               Enum.TryParse(classStr, out classCode);
                            }
                        }
                    }
                }
            }

            ClassId[] assemblyClasses = { ClassId.SA, ClassId.FA, ClassId.UA };

            // Figure out what the component is and make it
            if (assemblyClasses.Contains(classCode))
            {
                component = new NhfAssembly();
            }
            else
            {
                component = new NhfPart();
            }

            // Start setting properties on the component now that we know what it is
            component.ProjectNum = project;
            component.ClassCode = classCode;

            // Triage the class code and set all the properties we can
            // ReSharper disable once SwitchStatementMissingSomeCases

            string profileStr = "";
            string seqStr = "";
            string specialStr = "";

            switch (classCode)
            {
                case ClassId.FS:
                    if (!string.IsNullOrEmpty(component.ProjectNum))
                    {
                        tokens[0].TrySubstring(ProjectLen + ClassLen, ProfileLen, out profileStr);
                    }
                    tokens[0].TrySubstring(ProjectLen + ClassLen, tokens[0].Length - (ProjectLen + ClassLen), out specialStr);
                    component.Special = specialStr + (tokens.Length > 1 ? tokens[1] : "");
                    break;
                case ClassId.GL:
                case ClassId.ST:
                    if (tokens.Length > 2)
                    {
                        profileStr = tokens[1];
                        seqStr = tokens[2];
                        tokens[0].TrySubstring(ProjectLen + ClassLen, tokens[0].Length - (ProjectLen + ClassLen), out component.Special);
                    }
                    break;
                case ClassId.GZ:
                    // GZ1M000 - 00 # TYP SEALANT
                    //0000GZ000 - 00000 # TYPE ENGINEERED SILICONE GASKETS
                    if (string.IsNullOrEmpty(component.ProjectNum))
                    {
                        tokens[0].TrySubstring(ClassLen, tokens[0].Length - ClassLen, out component.Special);
                        seqStr = tokens[1];
                        break;
                    }
                    goto default;
                case ClassId.SA:
                case ClassId.FA:
                    // SA/FA differ from UA by categorization

#if VAULT2
                    // VAULT2 uses a standard 5 digit SA numbering scheme, similar to UAs
                    tokens[0].TrySubstring(ProjectLen + ClassLen, 2, out profileStr);
                    tokens[0].TrySubstring(ProjectLen + ClassLen + 2, 3, out seqStr);
#endif

#if VAULT3
                    // The VAULT3 SA scheme is SAXXX-##### where 
                    // XXX is a 3 digit SA identifier indicating the type or family, and 
                    // ##%## is a 5 digit sequential code
                    tokens[0].TrySubstring(tokens[0].Length - 3, ProfileLen, out profileStr);
                    if (tokens.Length > 1)
                    {
                        seqStr = tokens[1];
                    }
#endif
                    break;

                case ClassId.UA:
                    // 1003SA00001
                    tokens[0].TrySubstring(ProjectLen + ClassLen, tokens[0].Length - (ProjectLen + ClassLen), out seqStr);
                    if (tokens.Length > 1)
                    {
                        component.Special = tokens[1];
                    }
                    break;
                default:
                    // Everything that conforms to the 0000AA000-00000
                    if (tokens.Length == 2) {
                        tokens[0].TrySubstring(ProjectLen + ClassLen, tokens[0].Length - (ProjectLen + ClassLen), out profileStr);
                        seqStr = tokens[1];
                    }
                    break;
            }

            try
            {
                component.ProfileCode = Convert.ToInt32(profileStr);
            }
            catch {}

            try
            {
                component.SequentialNum = Convert.ToInt32(seqStr);
            }
            catch { }

            return component;
        }

        public string[] GetTokenizedPartNumber()
        {
            return TokenizePartNumber(PartNumberString(false));
        }

        public override string ToString()
        {
            return PartNumberString(false);
        }

        public string PartNumberString(bool dummySeq)
        {
            int seq = dummySeq ? 0 : SequentialNum;

            string[] componentStrings;
            switch (ClassCode)
            {
                case ClassId.SA:
                case ClassId.FA:
#if VAULT2
                    componentStrings = new[]
                    {
                        ProjectNum + ClassCode + ProfileCode.ToString("D" + 2) + seq.ToString("D" + 3)
                    };
#endif
#if VAULT3
                    componentStrings = new[]
                    {
                        ProjectNum + ClassCode + ProfileCode.ToString("D" + ProfileLen), seq.ToString("D" + 5)
                    };
#endif
                    break;
                case ClassId.UA:
                    componentStrings = new[]
                    {
                        ProjectNum + ClassCode + seq.ToString("D" + EngCodeLen)
                    };
                    if (!string.IsNullOrEmpty(Special))
                    {
                        componentStrings = componentStrings.Concat(new[] {Special}).ToArray();
                    }
                    break;
                case ClassId.FS:
                    if (ProjectNum != null)
                    {
                        // 1003FS001
                        componentStrings = new[]
                        {
                            ProjectNum + ClassCode + seq
                        };
                        break;
                    }
                    // FS1401-075
                    componentStrings = new[]
                    {
                        ClassCode + Special
                    };
                    break;
                case ClassId.GL:
                case ClassId.ST:
                    // 0000GL[T|Z]00 - 00 - 0000 # GLASS/STONE - MAKEUP - SHAPE - SEQ
                    componentStrings = new[]
                    {
                        ProjectNum + ClassCode + Special,
                        ProfileCode.ToString("D" + 2),
                        seq.ToString("D" + 4)
                    };
                    break;
                case ClassId.GZ:
                    // GZ1M000 - 00 # TYP SEALANT
                    if (ProjectNum == null)
                    {
                        componentStrings = new[]
                        {
                            ClassCode + Special,
                            seq.ToString("D" + 2)
                        };
                        break;
                    }
                    // 0000GZ000 - 00000 # TYPE ENGINEERED SILICONE GASKETS
                    goto default;
                default:
                    componentStrings = new[]
                    {
                        ProjectNum + ClassCode + ProfileCode.ToString("D" + ProfileLen),
                        seq.ToString("D" + EngCodeLen)
                    };
                    break;
            }

            return string.Join(_partDelim.ToString(), componentStrings);
        }


        /// <summary>
        ///     Decomposes a full Part Number string into its component Part and Eng Code
        /// </summary>
        /// <param name="partNumber"></param>
        /// <returns></returns>
        public static string[] TokenizePartNumber(string partNumber) => partNumber.Split(Resources.DELIMITER_DASH[0]);
    }
}