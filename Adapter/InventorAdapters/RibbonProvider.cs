﻿#region

using Inventor;

#endregion

namespace Inv.Common.Adapter.InventorAdapters
{
    public static class RibbonProvider
    {
        private const string DocTypeAssembly = "Assembly";
        private const string DocTypePart = "Part";

        public const string TabAssemble = "id_TabAssemble";
        public const string TabModel = "id_TabModel";
        public const string TabManage = "id_TabManage";
        public const string TabAddIns = "id_AddInsTab";

        private static UserInterfaceManager GetUiManager(this Application inventor)
        {
            return inventor.UserInterfaceManager;
        }

        private static RibbonTab GetRibbonTab(this Application inventor, string docType, string tabName)
        {
            return GetUiManager(inventor)?.Ribbons[docType].RibbonTabs[tabName];
        }

        public static RibbonTab GetAsmRibbonTab(this Application inventor, string tabName)
        {
            return GetRibbonTab(inventor, DocTypeAssembly, tabName);
        }

        public static RibbonTab GetPartRibbonTab(this Application inventor, string tabName)
        {
            return GetRibbonTab(inventor, DocTypePart, tabName);
        }

        public static RibbonPanel GetOrAddPanel(RibbonPanels ribbonPanels, string name, string internalName,
            string clientId)
        {
            RibbonPanel rp = null;
            try
            {
                rp = ribbonPanels[internalName];
                if (rp != null) return rp;
            }
            catch
            {
                // ignored
            }

            try
            {
                rp = ribbonPanels.Add(
                    name,
                    internalName,
                    clientId
                    );
            }
            catch
            {
                // ignored
            }

            return rp;
        }
    }
}