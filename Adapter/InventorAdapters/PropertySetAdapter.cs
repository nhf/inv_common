﻿#region

using System;
using System.Collections.Generic;
using Inv.Common._Core.Exceptions;
using Inventor;
using Edison.Interface;

#endregion

namespace Inv.Common.Adapter.InventorAdapters
{
    /// <summary>
    ///     The PropertySetAdapter provides simplified access to Inventor Document PropertySets and the
    ///     Properties that may be stored there.
    /// </summary>
    /// <remarks>
    ///     This is one of several DataSet Adapters that implements a single interface to the several disparate
    ///     data storage locations that Inventor provides.
    /// </remarks>
    /// <seealso cref="IDataSet" />
    /// <seealso cref="AttributeAdapter" />
    public sealed class PropertySetAdapter : IDataSet
    {
        // TODO: finish converting this Adapter to an IDataSet implementer
        // Inventor has a few default PropertySets. The keys for those are here for ease-of-access.

        public const string PropertysetsCustom = "Inventor User Defined Properties";
        public const string PropertysetsDesign = "Design Tracking Properties";
        public const string PropertysetsDocument = "Inventor Document Summary Information";
        public const string PropertysetsSummary = "Inventor Summary Information";

        private readonly PropertySet _propertySet;

        /// <summary>
        ///     Creates a PropertySetAdapter from a PropertySet.
        /// </summary>
        /// <param name="propertySet">The PropertySet to wrap.</param>
        public PropertySetAdapter(PropertySet propertySet)
        {
            _propertySet = propertySet;
        }

        /// <summary>
        ///     Creates a PropertySetAdapter from a Document and a PropertySet Key.
        /// </summary>
        /// <remarks>
        ///     See the Key strings in this class for default PropertySets that are available.
        /// </remarks>
        /// <param name="doc">The Document to get the PropertySet from.</param>
        /// <param name="propertySet">The PropertySet Key string.</param>
        /// <exception cref="PropertySetNotFoundException">
        ///     Thrown if the requested PropertySet does not exist.
        /// </exception>
        public PropertySetAdapter(Document doc, string propertySet)
        {
            try
            {
                _propertySet = doc.PropertySets[propertySet];
                if (_propertySet == null)
                {
                    throw new PropertySetNotFoundException(propertySet);
                }
            }
            catch (Exception)
            {
                throw new PropertySetNotFoundException(propertySet);
            }
        }

        /// <summary>
        ///     Creates a PropertySetAdapter from a Document and a PropertySet Key.
        /// </summary>
        /// <remarks>
        ///     See the Key strings in this class for default PropertySets that are available.
        /// </remarks>
        /// <param name="doc">The Document to get the PropertySet from.</param>
        /// <param name="propertySet">The PropertySet Key string.</param>
        /// <exception cref="PropertySetNotFoundException">
        ///     Thrown if the requested PropertySet does not exist.
        /// </exception>
        public PropertySetAdapter(IDocument idoc, string propertySet)
        {
            Document doc = idoc.Underlying();
            try
            {
                _propertySet = doc.PropertySets[propertySet];
                if (_propertySet == null)
                {
                    throw new PropertySetNotFoundException(propertySet);
                }
            }
            catch (Exception)
            {
                throw new PropertySetNotFoundException(propertySet);
            }
        }

        public string Name { get; set; }

        public object this[string key]
        {
            get { return !Contains(key) ? null : ((Property) Get(key)).Value; }
            set
            {
                if (value is decimal)
                {
                    value = Convert.ToDouble(value);
                }

                if (Contains(key))
                {
                    Property prop = _propertySet[key];
                    prop.Value = value;
                }
                else
                {
                    Add(key, value);
                }
            }
        }

        public object Get(string key)
        {
            try
            {
                return _propertySet[key];
            }
            catch
            {
                return null;
            }
        }

        public IDictionary<string, object> Items
        {
            get { throw new NotImplementedException(); }
        }

        public bool Contains(string key)
        {
            Property property;
            try
            {
                property = _propertySet[key];
                if (property == null)
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public void Add(string key, object val)
        {
            _propertySet.Add(val, key);
        }

        public void Delete(string key)
        {
            if (!Contains(key)) return;
            ((Property) Get(key)).Delete();
        }

        public bool IsEmpty()
        {
            throw new NotImplementedException();
        }
    }


    /**
    Internal use exception to throw when a Property does not exist.
    **/

    internal class PropertyNotFoundException : LoggableException
    {
        public PropertyNotFoundException(string item) : base("Inventor Property not found : " + item)
        {
        }
    }

    /**
    External use exception thrown when a bad PropertySet request is made (ie PropertySet DNE)
    **/

    public class PropertySetNotFoundException : LoggableException
    {
        public PropertySetNotFoundException(string set) : base("Inventor PropertySet not found : " + set)
        {
        }
    }
}