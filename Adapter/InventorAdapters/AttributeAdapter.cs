﻿#region

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Inventor;

#endregion

namespace Inv.Common.Adapter.InventorAdapters
{
    /// <summary>
    ///     The AttributeAdapter provides simplified access to Inventor AttributeSets and the Attributes
    ///     that may be stored there.
    /// </summary>
    /// <remarks>
    ///     This is one of several DataSet Adapters that implements a single interface to the several disparate
    ///     data storage locations that Inventor provides.
    ///     Attributes are special - almost any Inventor object can have AttributeSets so they can be used to store
    ///     a lot of information.
    /// </remarks>
    /// <seealso cref="IDataSet" />
    /// <seealso cref="PropertySetAdapter" />
    public class AttributeAdapter : IDataSet
    {
        private readonly AttributeSet _attributeSet;

        public AttributeAdapter(object obj, string name)
        {
            AttributeSets attrSets = null;
            try
            {
                attrSets = AttributeSets(obj);
            }
            catch
            {
                // Ignore 
            }

            if (attrSets == null) return;
            try
            {
                _attributeSet = attrSets.NameIsUsed[name] ? attrSets[name] : attrSets.Add(name);
            }
            catch
            {
                // ignore
            }
        }

        public string Name
        {
            get { return _attributeSet?.Name; }
            set
            {
                if (_attributeSet == null) return;
                _attributeSet.Name = value;
            }
        }

        public object this[string key]
        {
            get { return !Contains(key) ? null : ((Attribute) Get(key)).Value; }
            set
            {
                if (Contains(key))
                {
                    Attribute attr = _attributeSet[key];
                    attr.Delete();
                }
                Add(key, value);
            }
        }

        public object Get(string key)
        {
            return _attributeSet?[key];
        }

        public IDictionary<string, object> Items
        {
            get
            {
                try
                {
                    return _attributeSet?.Cast<Attribute>().ToDictionary(attr => attr.Name, attr => attr.Value);
                }
                catch
                {
                    return new Dictionary<string, object>();
                }
            }
        }

        public bool Contains(string key)
        {
            try
            {
                if (_attributeSet == null) return false;
                return _attributeSet.NameIsUsed[key];
            }
            catch
            {
                return false;
            }
        }

        public void Add(string key, object val)
        {
            ValueTypeEnum vType = 0;
            if (val is bool)
            {
                // Apparently ValueTypeEnum.kBooleanType isn't valid - seems to crash. TODO: investigate;
                val = (bool) val ? 1 : 0;
                vType = ValueTypeEnum.kIntegerType;
            }
            else if (val is int)
            {
                vType = ValueTypeEnum.kIntegerType;
            }
            else if (val is double)
            {
                vType = ValueTypeEnum.kDoubleType;
            }
            else if (val is string)
            {
                vType = ValueTypeEnum.kStringType;
            }
            else if (val is byte[])
            {
                vType = ValueTypeEnum.kByteArrayType;
            }

            if (vType == 0) return;

            _attributeSet.Add(key, vType, val);
        }

        public void Delete(string key)
        {
            if (!Contains(key)) return;
            ((Attribute) Get(key)).Delete();
        }

        public bool IsEmpty()
        {
            try
            {
                return _attributeSet == null || _attributeSet.Count == 0;
            }
            catch
            {
                return true;
            }
        }

        public static AttributeSets AttributeSets(object obj)
        {
            return obj.GetType().InvokeMember(
                "AttributeSets",
                BindingFlags.GetProperty,
                null,
                obj,
                null,
                null, null, null) as AttributeSets;
        }

        public static bool Exists(Document doc, string name)
        {
            AttributeSets attrSets = doc.AttributeSets;
            return attrSets.NameIsUsed[name];
        }
    }
}