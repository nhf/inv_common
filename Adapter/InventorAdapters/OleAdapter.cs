﻿using Inventor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv.Common.Adapter.InventorAdapters
{
    public class OleAdapter : IDataSet
    {
        private ReferencedOLEFileDescriptors _refOleFiles;

        public OleAdapter(ReferencedOLEFileDescriptors refOleFiles)
        {
            _refOleFiles = refOleFiles;
        }

        public OleAdapter(Document doc)
        {
            _refOleFiles = doc.ReferencedOLEFileDescriptors;
        }

        public object this[string key]
        {
            get
            {
                return Get(key);
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public IDictionary<string, object> Items
        {
            get
            {
                IDictionary<string, object> itemsDict = new Dictionary<string, object>();
                foreach (ReferencedOLEFileDescriptor refOle in _refOleFiles)
                {
                    itemsDict.Add(refOle.FullFileName, refOle);
                }
                return itemsDict;
            }
        }

        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Add(string key, object val)
        {
            if (val.GetType() != typeof(string))
            {
                throw new ArgumentException();
            }
            _refOleFiles.Add((string) val);
        }

        public bool Contains(string key)
        {
            try
            {
                var a = _refOleFiles.ItemByName[key];
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Delete(string key)
        {
            object o = this[key];
            ReferencedOLEFileDescriptor refOle = o as ReferencedOLEFileDescriptor;
            refOle?.Delete();
        }

        public object Get(string key)
        {
            if (Contains(key))
            {
                return _refOleFiles.ItemByName[key];
            }
            return null;
        }

        public bool IsEmpty()
        {
            return _refOleFiles.Count == 0;
        }
    }
}
