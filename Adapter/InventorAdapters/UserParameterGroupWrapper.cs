﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Inv.Common._Core.Exceptions;
using Inv.Common._Core.Extensions;
using Inventor;

#endregion

namespace Inv.Common.Adapter.InventorAdapters
{
    /// <summary>
    ///     The ParameterWrapper provides simplified access to Inventor Document Parameters and the values
    ///     that may be stored there.
    /// </summary>
    /// <remarks>
    ///     Due to the strange way Parameters are grouped and stored in Inventor this needs to be Generic because the Parameter
    ///     Groups are not siblings.
    ///     Allowable types are: ModelParameters, ReferenceParameters, UserParameters, and CustomParameterGroup.
    ///     This is one of several DataSet Adapters that implements a single interface to the several disparate
    ///     data storage locations that Inventor provides.
    /// </remarks>
    /// <seealso cref="IDataSet" />
    /// <seealso cref="PropertySetAdapter" />
    /// <seealso cref="AttributeAdapter" />
    public class UserParameterGroupWrapper : IDataSet
    {
        private readonly UserParameters _parameters;

        public UserParameterGroupWrapper(Document doc)
        {
            if (doc.IsAssembly())
            {
                _parameters = ((AssemblyDocument) doc).ComponentDefinition.Parameters.UserParameters;
            }
            else if (doc.IsPart())
            {
                _parameters = ((PartDocument) doc).ComponentDefinition.Parameters.UserParameters;
            }
            else
            {
                throw new BadDocumentTypeException(DocumentTypeEnum.kAssemblyDocumentObject, doc.DocumentType);
            }
        }

        public UserParameterGroupWrapper(UserParameters parameters)
        {
            _parameters = parameters;
        }

        public string Name
        {
            get { return _parameters.ToString(); }
            set { }
        }

        public object this[string key]
        {
            get { return !Contains(key) ? null : ((UserParameter) Get(key)).Value; }
            set
            {
                if (Contains(key))
                {
                    UserParameter param = _parameters[key];
                    param.Value = value;
                    return;
                }
                Add(key, value);
            }
        }

        public object Get(string key)
        {
            return _parameters[key];
        }

        public IDictionary<string, object> Items
        {
            get
            {
                return _parameters.Cast<UserParameter>()
                    .ToDictionary<UserParameter, string, object>(param => param.Name, param => param);
            }
        }

        public bool Contains(string key)
        {
            var exists = false;
            try
            {
                UserParameter ignore = _parameters[key];
                exists = true;
            }
            catch
            {
                // ignored
            }
            return exists;
        }

        public void Add(string key, object val)
        {
            UnitsTypeEnum units;
            if (val is bool)
            {
                units = UnitsTypeEnum.kBooleanUnits;
            }
            else if (val is double || val is decimal)
            {
                units = UnitsTypeEnum.kDatabaseLengthUnits;
            }
            else
            {
                units = UnitsTypeEnum.kTextUnits;
            }
            _parameters.AddByValue(key, val, units);
        }

        public void Delete(string key)
        {
            if (!Contains(key)) return;
            ((UserParameter) Get(key)).Delete();
        }

        public bool IsEmpty()
        {
            return _parameters == null || _parameters.Count == 0;
        }
    }
}