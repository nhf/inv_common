﻿#region

using System;
using System.Runtime.InteropServices;
using Inventor;

#endregion

namespace Inv.Common.Adapter.InventorAdapters
{
    public static class InventorProvider
    {
        /// <summary>
        ///     Attech to an Inventor instance from out of process.
        /// </summary>
        /// <param name="createIfNone">Indicates to create an Inventor instance if a running one is not found</param>
        /// <returns></returns>
        public static Application GetInstanceOutOfProcess(bool createIfNone)
        {
            Application inv = null;

            // Try to get an active instance of Inventor
            try
            {
                inv = Marshal.GetActiveObject("Inventor.Application") as Application;
                if (createIfNone && inv == null)
                {
                    // If not active, create a new Inventor session
                    Type inventorAppType = Type.GetTypeFromProgID("Inventor.Application");
                    inv = Activator.CreateInstance(inventorAppType) as Application;
                }
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Failed to start Inventor");
            }
            return inv;
        }

        /// <summary>
        ///     Gets an Apprentice instance
        /// </summary>
        /// <returns></returns>
        public static ApprenticeServerComponent GetApprentice()
        {
            return new ApprenticeServerComponent();
        }

        /// <summary>
        ///     Get Inventor from in-process (ie from an Add-In).
        /// </summary>
        /// <param name="addInSiteObject">The in-process Add-In hook.</param>
        /// <returns></returns>
        public static Application GetInstanceInProcess(ApplicationAddInSite addInSiteObject)
        {
            return addInSiteObject.Application;
        }

        public static void Finish(Application inv)
        {
            inv?.Quit();
        }
    }
}