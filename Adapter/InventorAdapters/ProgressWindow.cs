﻿using Inventor;

namespace Inv.Common.Adapter.InventorAdapters
{
    public class ProgressWindow : IProgressIndicator
    {
        private readonly Application _inv;
        private ProgressBar _progressBar;

        private string _text;
        private int _total;
        private int _progress;

        public ProgressWindow(Application inv, string title)
        {
            _text = title;
            _inv = inv;
        }

        protected ProgressWindow() { }

        public void Create(int total, string text, int progress = 0, bool autoFinish = true)
        {
            _total = total;
            _text = text;
            _progressBar = _inv?.CreateProgressBar(false, _total, _text);

            _progress = 0;
            if (progress > 0) UpdateProgress(progress);
        }

        public void Finish()
        {
            _progressBar?.Close();
        }

        public void Increment()
        {
            _progress++;

            if (_progress < _total)
            {
                _progressBar?.UpdateProgress();
                return;
            }

            Finish();
        }

        public void UpdateProgress(int newProgress)
        {
            while (_progress > newProgress)
            {
                Increment();
            }
        }

        public void AddToTotal(int additional)
        {
            _total += additional;
            Finish();
            Create(_total, _text, _progress);
        }
    }
}
