﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Inventor;
using Path = System.IO.Path;

#endregion

namespace Inv.Common.Adapter.InventorAdapters
{
    public class BomWrapper
    {
        // TODO: Generify this when creating the full API wrapper
        private const string BomviewParts = "Parts Only";
        private const string BomviewModel = "Unnamed";
        private readonly AssemblyDocument _asm;
        private readonly BOM _bom;

        public BomWrapper(AssemblyDocument doc)
        {
            _asm = doc;
            _bom = doc.ComponentDefinition.BOM;
        }

        public void SetBomStructure(BOMStructureEnum bomStructure = BOMStructureEnum.kDefaultBOMStructure)
        {
            _asm.ComponentDefinition.BOMStructure = bomStructure;
        }

        private BOMView GetPartsBomView()
        {
            _bom.PartsOnlyViewEnabled = true;
            return _bom.BOMViews[BomviewParts];
        }

        private BOMView GetModelBomView()
        {
            return _bom.BOMViews[BomviewModel];
        }

        public void SetQty(IDictionary<string, decimal> partsQty, bool basePart = true)
        {
            SetQty(partsQty.ToDictionary(kvp => kvp.Key, kvp => Convert.ToString(kvp.Value)), basePart);
        }

        public void SetQty(IDictionary<string, string> partsQty, bool basePart = true)
        {
            BOMView view = basePart ? GetPartsBomView() : GetModelBomView();

            foreach (BOMRow row in view.BOMRows)
            {
                if (partsQty.Count == 0)
                {
                    return;
                }

                ComponentDefinition componentDef = row.ComponentDefinitions[1];
                var doc = (Document) componentDef.Document;
                string fullPath = doc.FullDocumentName;
                string docName = Path.GetFileNameWithoutExtension(fullPath);

                if (!partsQty.ContainsKey(docName)) continue;

                double newBomQty = double.Parse(partsQty[docName]);
                row.TotalQuantity = ((int) Math.Ceiling(newBomQty)).ToString();
                partsQty.Remove(docName);
            }
        }

        private void SetQty(string part, string qty)
        {
            var partsQty = new Dictionary<string, string> {{part, qty}};
            SetQty(partsQty);
        }

        private void SetQty(IEnumerable<string> parts, string qty)
        {
            Dictionary<string, string> partsQty = parts.ToDictionary(part => part, part => qty);
            SetQty(partsQty);
        }

        public void SetQty(List<string> parts, List<string> qtys)
        {
            var partsQty = new Dictionary<string, string>();
            List<string>.Enumerator partEnumerator = parts.GetEnumerator();
            List<string>.Enumerator qtyEnumerator = qtys.GetEnumerator();

            while (partEnumerator.MoveNext() && qtyEnumerator.MoveNext())
            {
                if (partEnumerator.Current != null)
                {
                    partsQty.Add(partEnumerator.Current, qtyEnumerator.Current);
                }
            }
            SetQty(partsQty);
        }
    }
}