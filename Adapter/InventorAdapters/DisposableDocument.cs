﻿#region

using System;
using Inv.Common._Core.Extensions;
using Inventor;

#endregion

namespace Inv.Common.Adapter.InventorAdapters
{
    public class DisposableDocument : IDisposable
    {
        private bool disposed;
        public Document Doc;

        public DisposableDocument(Document doc)
        {
            Doc = doc;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public static bool TryLoadDocument(Application inventor, string fullPath,
            out DisposableDocument disposableDocument, bool visible=false)
        {
            Document doc;
            bool loads = inventor.TryLoadDocument(fullPath, out doc, visible);

            if (loads)
            {
                disposableDocument = new DisposableDocument(doc);
                return true;
            }

            disposableDocument = null;
            return false;
        }

        ~DisposableDocument()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            /*
             * Cleanup the unmananged references when this object is destroyed. 
             * Otherwise Inventor will just keep holding these open documents indefinitely
             */

            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                // nothing to free at the moment
            }

            if (Doc != null)
            {
                Doc.Close(true);
            }

            disposed = true;
        }
    }
}